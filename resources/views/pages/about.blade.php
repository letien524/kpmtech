@extends('index')

@section('content')
    @include('components.banner-style',['banner'=>@$page['banner']])

    <div class="sub full_container visual01" >
        <div class="container">

            @include('section.header')

            @include('components.breadcrumb',['breadcrumb'=>$breadcrumb])

            <div class="container contents_warp">
                <div class="contents_side">
                    <div class="sub_contents design introducte01">

                        <h1 class="sr-only">{{$metaTitle}}</h1>


                        @if(count((array)@$page['content']['management_philosophy']))
                            <div>
                                <div class="location02">
                                    <h2 class="title">{{__('Triết lý quản lý')}}</h2>
                                </div>
                                @foreach($page['content']['management_philosophy'] as $management_philosophy)
                                    <div class="group01">
                                        <strong>{{$management_philosophy['name']}}</strong>
                                        <p>{{$management_philosophy['content']}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <hr/>
                        @endif
                    </div>


                    @if(count((array)@$page['content']['history']))
                        <div class="sub_contents design history">
                            <div class="location02">
                                <h2 class="title">{{__('Lịch sử công ty')}}</h2>
                            </div>

                            <div class="mobile">
                                <div class="clearfix"></div>
                                @foreach($page['content']['history'] as $history)
                                    <h3 class="history_title">
                                        <span class="month"> {{$history['date']}}</span>
                                    </h3>
                                    <div><p>{{$history['content']}}</p></div>
                                    <div class="clearfix"></div>
                                @endforeach
                            </div>
                            <hr>
                        </div>

                    @endif

                    @if(strlen(@$page['content']['organization_structure']))
                        <div class="sub_contents design group">
                            <div class="location02">
                                <h2 class="title">{{__('Cơ cấu tổ chức')}}</h2>
                            </div>

                            {!! $page['content']['organization_structure'] !!}

                        </div>
                    @endif


                </div>
            </div>

            @include('components.go-to-top')
        </div>
    </div>
@endsection