<?php

    namespace App\Http\Controllers;

    use App\Blog;
    use App\Contact;
    use App\Jobs\MailToAdminWithNewCustomerJob;
    use App\Product;
    use App\ProductCategory;
    use App\Page;
    use Illuminate\Http\Request;

    class SiteController extends Controller
    {

        public function __construct()
        {
            $this->middleware('lang');
        }


        private $langActive = [
            'vi',
            'en',
            'ko',
        ];

        public function changeLang($lang)
        {
            if (in_array($lang, $this->langActive)) {
                session(['lang' => $lang]);
                return back();
            }
            return abort(404);
        }

        public function home()
        {
            $home = Page::where('type', 'home')->first();

            if (empty($home)) {
                return abort(404);
            }

            $blogs = Blog::isDisplay()->latest()->take(6)->get(['name','slug','excerpt','created_at']);

            //seo
            $metaDescription = $home['meta_description'];
            $metaTitle = $home['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : __("Trang chủ");

            $data = [
                'home' => $home,
                'blogs' => $blogs,
                'metaTitle' => $metaTitle,
            ];



            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage($home['meta_image']);

            return view('pages.home', $data);
        }

        public function blogs()
        {

            $page = Page::where(['type' => 'blog'])->first();

            if(empty($page)){
                return abort(404);
            }

            $blogs = Blog::isDisplay()->latest()->paginate(12);

            $breadcrumb = [
                [
                    'name'=>__('Tin tức'),
                    'link'=>route('blog'),
                ]
            ];

            $data = [
                'blogs' => $blogs,
                'page' => $page,
                'breadcrumb' => $breadcrumb,
            ];

            //seo
            $metaDescription = $page['meta_description'];
            $metaTitle = $page['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : __("Tin tức");

            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage($page['meta_image']);

            return view('pages.blogs', $data);
        }

        public function blogDetails($slug)
        {
            $page = Page::where(['type' => 'blog'])->first();

            if(empty($page)){
                return abort(404);
            }

            $blog = Blog::isDisplay()->where('slug', $slug)->first();

            if (empty($blog)) {
                return abort(404);
            }

            $relatedBlogs = Blog::isDisplay()->where('id', '<>', $blog['id'])->inRandomOrder()->take(5)->get();

            $breadcrumb = [
                [
                    'name'=>__('Tin tức'),
                    'link'=>route('blog'),
                ],
                [
                    'name'=> $blog['name'],
                    'link'=>route('blog.detail',$blog['slug']),
                ]
            ];

            $data = [
                'blog' => $blog,
                'relatedBlogs' => $relatedBlogs,
                'page' => $page,
                'breadcrumb' => $breadcrumb,
            ];

            //seo
            $metaDescription = $blog['meta_description'];
            $metaTitle = $blog['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : $blog[getColumnByLang('name')];

            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage(strlen($blog['meta_image']) ? $blog['meta_image'] : $blog['image']);

            return view('pages.blog-details', $data);
        }

        public function services()
        {

            $page = Page::isDisplay()->where(['type' => 'service'])->first();

            if(empty($page)){
                return abort(404);
            }

            $services = Service::isDisplay()->latest()->get();

            $data = [
                'services' => $services,
                'page' => $page,
            ];

            //seo
            $metaDescription = $page[getColumnByLang('meta_description')];
            $metaTitle = $page[getColumnByLang('meta_title')];
            $metaTitle = strlen($metaTitle) ? $metaTitle : __("Dịch vụ");

            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage($page['meta_image']);

            return view('pages.services', $data);
        }

        public function serviceDetails($slug)
        {
            $page = Page::isDisplay()->where(['type' => 'blog'])->first();

            if(empty($page)){
                return abort(404);
            }

            $service = Service::isDisplay()->where('slug', $slug)->first();

            if (empty($service)) {
                return abort(404);
            }

            $relatedServices = Service::isDisplay()->where('id', '<>', $service['id'])->inRandomOrder()->take(3)->get();

            $data = [
                'service' => $service,
                'relatedServices' => $relatedServices,
                'page' => $page,
            ];

            //seo
            $metaDescription = $service[getColumnByLang('meta_description')];
            $metaTitle = $service[getColumnByLang('meta_title')];
            $metaTitle = strlen($metaTitle) ? $metaTitle : $service[getColumnByLang('name')];

            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage(strlen($service['meta_image']) ? $service['meta_image'] : $service['image']);

            return view('pages.service-details', $data);
        }

        public function productCategory($slug)
        {

            $productCategory = ProductCategory::isDisplay()->where(['slug' => $slug])->first();


            if(empty($productCategory)){
                return abort(404);
            }

            $products = $productCategory->products()->where('display',1)->latest()->paginate(16);

            $breadcrumb = [
               [
                   'name' => $productCategory['name'],
                   'link' => route('product.category',$productCategory['slug']),
               ]
            ];

            //seo
            $metaDescription = $productCategory['meta_description'];
            $metaTitle = $productCategory['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : $productCategory['name'];

            $data = [
                'products' => $products,
                'productCategory' => $productCategory,
                'breadcrumb' => $breadcrumb,
                'metaTitle' => $metaTitle,
            ];


            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage($productCategory['meta_image']);

            return view('pages.product-category', $data);
        }

        public function productDetail($slug)
        {
            $product = Product::isDisplay()->where(['slug' => $slug])->first();
            $product->load('categories');
            if(empty($product)){
                return abort(404);
            }

            $productCategories = count($product->categories) ? $product->categories->pluck('id')->toArray() : [];

            $relatedProducts = Product::where('id','<>',$product['id'])->where(function($query) use($productCategories){
                $query->whereHas('categories', function($query) use($productCategories){
                    $query->whereIn('category_id',$productCategories);
                });
            })->inRandomOrder()->take(8)->get();

            $breadcrumb = [
                [
                    'name' => @$product->categories[0]['name'],
                    'link' => route('product.category',@$product->categories[0]['slug']),
                ],
                [
                    'name' => $product['name'],
                    'link' => route('product.detail',$product['slug']),
                ]

            ];

            $data = [
                'product' => $product,
                'relatedProducts' => $relatedProducts,
                'breadcrumb' => $breadcrumb,
            ];

            //seo
            $metaDescription = $product['meta_description'];
            $metaTitle = $product['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : $product['name'];

            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage(strlen($product['meta_image']) ? $product['meta_image'] : $product['image']);

            return view('pages.product-details', $data);
        }

        public function about()
        {

            $page = Page::where('type', 'about')->first();


            if (empty($page)) {
                return abort(404);
            }

            $breadcrumb = [
                [
                    'name' => __("Giới thiệu"),
                    'link' => route('about'),
                ],

            ];

            //seo
            $metaDescription = $page['meta_description'];
            $metaTitle = $page['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : __("Giới thiệu");

            $data = [
                'page' => $page,
                'breadcrumb' => $breadcrumb,
                'metaTitle' => $metaTitle,
            ];



            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage($page['meta_image']);

            return view('pages.about', $data);
        }

        public function contact()
        {

            $page = Page::where('type', 'contact')->first();

            if (empty($page)) {
                return abort(404);
            }

            $breadcrumb = [
                [
                    'name' => __("Liên hệ"),
                    'link' => route('contact'),
                ],

            ];

            //seo
            $metaDescription = $page['meta_description'];
            $metaTitle = $page['meta_title'];
            $metaTitle = strlen($metaTitle) ? $metaTitle : __("Liên hệ");

            $data = [
                'page' => $page,
                'breadcrumb' => $breadcrumb,
                'metaTitle' => $metaTitle,
            ];



            seoTitle($metaTitle);
            seoDescription($metaDescription);
            seoImage($page['meta_image']);

            return view('pages.contact', $data);
        }

        public function contactSubmit(Request $request)
        {

            $this->validate($request, [
                'name' => 'required|max:50',
                'email' => 'required|email',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'address' => 'required',

            ], [
                'name.required' => __('Trường họ tên không được bỏ trống'),
                'name.max' => __('Trường họ tên không dài quá 50 ký tự'),
            ]);


            $input = $request->all();

            $customer = Contact::create($input);

            $this->dispatch(new MailToAdminWithNewCustomerJob($customer));

            return back()->with([
                'flash_level' => 'success',
                'flash_message' => __('Liên hệ của bạn được gửi đi thành công!')
            ]);


        }


        public function contactRegister(Request $request)
        {

            $this->validate($request, [
                'email' => 'required|email',
            ], []);


            $input = $request->all();

            $customer = Customer::create($input);

            $this->dispatch(new MailToAdminWithNewCustomerJob($customer));

            return back()->with([
                'flash_level' => 'success',
                'flash_message' => __('Liên hệ của bạn được gửi đi thành công!')
            ]);


        }
    }
