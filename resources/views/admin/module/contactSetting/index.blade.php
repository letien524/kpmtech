@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',config('admin.method.index.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{{ route($module['model'].".bulkDestroy") }}" method="post">
        {{ csrf_field() }}
        {{ method_field('delete') }}
        <div class="box">
            <div class="box-header with-border">
                <button type="submit" class="btn btn-danger" onclick="return confirm('{{ __('Bạn có chắc chắn xóa không ?') }}')"><i class="fa fa-trash-o"></i> {{ __('Xóa') }}</button>
                <a href="{{ route($module['model'].".create") }}" class="btn btn-success"><i class="fa fa-plus"></i> {{ __('Thêm') }}</a>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                @include('admin.components.table')
            </div>
            <!-- /.box-body -->
        </div>
    </form>

    @include('admin.components.destroy-confirm')

@endsection