
<style>
    @if(!empty(@$banner))

        @if(is_array($banner))
            @if(strlen(@$banner['mobile']))
                body.english .sub.full_container {
                    background-image: url({{imageUrlRender($banner['mobile'])}}) !important;
                }
            @endif

            @if(strlen(@$banner['desktop']))
                @media (min-width: 1171px){
                    body.english .sub.full_container {
                        background-image: url({{imageUrlRender($banner['desktop'])}}) !important;
                    }
                }
            @endif
        @else
            body.english .sub.full_container {
                background-image: url({{imageUrlRender($banner)}}) !important;
            }

        @endif

    @endif

</style>

