<?php

return [
    'services' => 'services',
    'service_details' => 'services/{slug}',
    'events' => 'events',
    'event_details' => 'events/{slug}',
    'rooms' => 'rooms',
    'room_details' => 'rooms/{slug}',
    'blogs' => 'blogs',
    'blog_details' => 'blogs/{slug}',
    'contact_us' => 'contact-us',
    'about_us' => 'about-us',
];
