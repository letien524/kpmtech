@if(view()->exists("admin.components.table-row-loop.{$column}"))
    @include("admin.components.table-row-loop.{$column}")
@else
    <td>{{ is_array($item[$column]) ? @$item[$column]['vi'] : $item[$column] }}</td>
@endif
