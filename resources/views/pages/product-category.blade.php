@extends('index')
@section('content')
    @include('components.banner-style',['banner'=>@$productCategory['banner']])

    <div class="sub full_container visual01">
        <div class="container">
            @include('section.header')

            @include('components.breadcrumb',['breadcrumb'=>$breadcrumb])

            <div class="container contents_warp">
                <div class="contents_side">
                    <div class="sub_contents design map">
                        <div class="location02">
                            <h1 class="sr-only">{{ $metaTitle }}</h1>
                            <h2 class="title">{{ $productCategory['name'] }}</h2>
                        </div>

                        <div class="row vk-shop__list">
                            @foreach($products as $product)
                                @include('components.product',['$product' => $product])
                            @endforeach

                        </div>

                        {!! $products->links() !!}

                    </div>
                </div>
            </div>

            @include('components.go-to-top')

        </div>
    </div>

@endsection