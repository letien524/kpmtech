<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover data-table">
        <thead>
        <tr>
            <th style="width: 30px;">
                <input type="checkbox" name="selectAll" onchange="inputSelectAll(this)">
            </th>
            <th style="width: 30px;">No.</th>
            @foreach(@$module['table'] as $column => $name)
                <th>{!! $name !!}</th>
            @endforeach
            <th style="width: 150px;text-align: center;">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        @foreach(@$data as $index => $item)
            <tr>
                <td><input type="checkbox" name="selectItem[]" value="{{ $item->id }}"></td>
                <td>{{ $loop->iteration }}</td>
                @foreach($module['table'] as $column => $name)
                    @include("admin.components.table-row-loop.row")
                @endforeach
                <td class="text-center">
                    <a href="{{ route($module['model'].".edit",$item) }}" title="Sửa"> <i class="fa fa-pencil fa-fw"></i> Sửa</a> |
                    <a href="{{ route($module['model'].".destroy",$item) }}"
                       onclick="itemDeleteConfirm(this,event)"
                       data-target="#detroyConfirm" data-toggle="modal" class="text-danger" title="Xóa">
                        <i class="fa fa-trash-o fa-fw"></i> Xóa
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

