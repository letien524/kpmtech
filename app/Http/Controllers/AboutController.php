<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class AboutController extends AdminController
{
    protected function fields(){

        return [];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'page';

    protected function module(){
        return [
            'name' => 'Giới thiệu',
            'model' => 'about',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Page::firstOrCreate(['type'=>'about']);
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }



    public function update(Request $request, Page $about)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $about->update($input);

        return back()->with($this->flashMessages);
    }
}
