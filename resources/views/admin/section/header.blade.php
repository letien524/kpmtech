<header class="main-header">
    <a href="@yield('module.show')" target="_blank" class="logo" title="@yield('module.show')">
        <span class="logo-mini"><b>W</b>eb</span>
        <span class="logo-lg"><b>Xem website</b></span>
    </a>
    <nav class="navbar navbar-static-top">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                @if(auth()->check())
                    <li class="user user-menu">
                        <a href="{!! route('user.edit',getAuthField('id')) !!}" title="Chỉnh sửa tài khoản">
                            <img src="{{ imageUrlRender(getAuthField('image')) }}" class="user-image" alt="{{ getAuthField('name') }}">
                            {{ getAuthField('name') }}
                        </a>
                    </li>
                @endif

                <li class="dropdown user user-menu">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();  document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i>
                        <span class="hidden-xs">Đăng xuất</span>
                    </a>
                </li>
            </ul>
        </div>

    </nav>
</header>