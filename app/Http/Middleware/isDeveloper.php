<?php

namespace App\Http\Middleware;

use Closure;

class isDeveloper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isDeveloper() && !isAdmin()){
            return redirect()->route('dashboard.index')->with([
                'flash_level' => 'error',
                'flash_message' => 'Permission Denied!',
            ]);
        }

        return $next($request);
    }
}
