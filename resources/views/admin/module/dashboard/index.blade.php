@extends('admin.admin')
@section('module.name',$module['name'])
{{--@section('module.page',config('admin.method.index.display_name'))--}}
{{--@section('module.index',route($module['model'].'.index'))--}}
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')


    <div class="row">
        <div class="col-md-4">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sản phẩm</span>
                    <span class="info-box-number">{!! number_format(@$products) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! @$products ? @$shownProducts / @$products * 100 : 100!!}%"></div>
                    </div>
                    <span class="progress-description">
                     {!! @$products - @$shownProducts !!} ẩn
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('product.index') !!}" class="_link" title="{!! @$products - @$shownProducts !!} ẩn"></a>
            </div>
        </div>

        <div class="col-md-4">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-address-card-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Liên hệ</span>
                    <span class="info-box-number">{!! number_format(@$contacts) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! @$contacts ? 1 - @$newContacts / @$contacts * 100 : 100!!}%"></div>
                    </div>
                    <span class="progress-description">
                    {!! @$newContacts !!} liên hệ mới
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('contact.index') !!}" class="_link" title="{!! @$newContacts !!} liên hệ mới"></a>
            </div>

        </div>


        <div class="col-md-4">
            <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tin tức</span>
                    <span class="info-box-number">{!! number_format(@$blogs) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! @$blogs ? @$shownBlogs / @$blogs * 100 : 100 !!}%"></div>
                    </div>
                    <span class="progress-description">
                   {!! @$blogs - @$shownBlogs !!} ẩn
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('blog.index') !!}" class="_link" title="{!! @$blogs - @$shownBlogs !!} ẩn"></a>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        {{-- <div class="panel-heading"></div> --}}
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Tiêu đề</th>
                        <th>Liên kết mẫu</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Trang chủ</td>
                        <td>
                            <a href="{{route('home')}}" target="_blank">/</a>
                        </td>
                    </tr>


                    <tr>
                        <td>Giới thiệu</td>
                        <td>
                            <a href="{{route('about')}}" target="_blank">/gioi-thieu</a>
                        </td>
                    </tr>


                    <tr>
                        <td>Tin tức</td>
                        <td>
                            <a href="{{route('blog')}}" target="_blank">/tin-tuc</a>
                        </td>
                    </tr>

                    <tr>
                        <td>Liên hệ</td>
                        <td>
                            <a href="{{route('contact')}}" target="_blank">/lien-he</a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection