<h3>Bạn vừa nhận được 1 liên hệ mới từ <a href="{{url('')}}">Website</a></h3>
<table>
    <tr>
        <th style="text-align: left">Khách hàng: </th>
        <td>{{$customer->name}}</td>
    </tr>
    <tr>
        <th style="text-align: left">Email: </th>
        <td>{{$customer->email}}</td>
    </tr>
    <tr>
        <th style="text-align: left">Số điện thoại: </th>
        <td>{{$customer->phone}}</td>
    </tr>

    <tr>
        <th style="text-align: left">Địa chỉ: </th>
        <td>{{$customer->address}}</td>
    </tr>

    <tr>
        <th style="text-align: left">Thời gian đăng ký: </th>
        <td>{{$customer->created_at}}</td>
    </tr>
    <tr>
        <th style="text-align: left">Nội dung: </th>
        <td>{{$customer->content}}</td>
    </tr>
</table>

<p><a href="{{route('contact.edit',$customer)}}">Chi tiết</a></p>