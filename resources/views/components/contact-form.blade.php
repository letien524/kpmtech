
@if(request()->route()->getName() == 'home')
    <form class="form_contact wow fadeInRight" data-wow-delay="0.2s" id="contacthome" method="post" action="{{route('contact')}}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <input type="text" placeholder="{{__('Họ tên')}}" name="name" value="{{old('name')}}" required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <input type="text" placeholder="{{__('Đơn vị công tác')}}" name="organization" value="{{old('organization')}}" required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <input type="email" placeholder="{{__('Email')}}" name="email" value="{{old('email')}}"  required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <input type="tel" placeholder="{{__('Số điện thoại')}}" name="phone" value="{{old('phone')}}"  required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <select required name="event_id">
                        <option value="">{{__('Sự kiện')}}</option>
                        @foreach($events as $event)
                            <option value="{{$event['id']}}" {{ old('event_id') == $event['id'] ? 'selected' : null }}>{{$event[getColumnByLang('name')]}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <!-- <input id="datepicker" width="276" /> -->
                    <div class="input-group date">
                        <input type="text" placeholder="{{__('Ngày tổ chức')}}" class="form-control" id="datepicker" name="date" value="{{old('date')}}">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item_input">
                    <textarea placeholder="{{__('Nội dung')}}" rows="3" name="content">{!! old('content') !!}</textarea>
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn_l">{{__('Đăng ký')}}</button>
            </div>
        </div>
    </form>
@else
    <form class="form_contact" id="contacthome" method="post" action="{{route('contact')}}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <label>{{__('Họ tên')}}</label>
                    <input type="text" name="name" value="{{old('name')}}" required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <label>{{__('Đơn vị công tác')}}</label>
                    <input type="text" name="organization" value="{{old('organization')}}" required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <label>{{__('Email')}}</label>
                    <input type="email" name="email" value="{{old('email')}}"  required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <label>{{__('Số điện thoại')}}</label>
                    <input type="tel" name="phone" value="{{old('phone')}}"  required>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <label>{{__('Chọn sự kiện')}}</label>
                    <select required name="event_id">
                        <option value="">{{__('Sự kiện')}}</option>
                        @foreach($events as $event)
                            <option value="{{$event['id']}}" {{ old('event_id') == $event['id'] ? 'selected' : null }}>{{$event[getColumnByLang('name')]}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="item_input">
                    <!-- <input id="datepicker" width="276" /> -->
                    <div class="input-group date">
                        <label>{{__('Ngày tổ chức')}}</label>
                        <input type="text" class="form-control" id="datepicker" name="date" value="{{old('date')}}">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item_input">
                    <label>{{__('Nội dung')}}</label>
                    <textarea placeholder="" rows="3" name="content">{!! old('content') !!}</textarea>
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn_l">{{__('Gửi')}}</button>
            </div>
        </div>
    </form>
@endif