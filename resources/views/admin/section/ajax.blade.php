<script>
    function ajaxDisplayChange(el, url){
        $.ajax({
            type: "put",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {},
            success: function (data) {
                $(el).closest('td').replaceWith(data.html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });
    }

    function ajaxFeatureChange(el, url){
        $.ajax({
            type: "put",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {},
            success: function (data) {
                $(el).closest('td').replaceWith(data.html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });
    }
</script>