<?php
    return [
        'index' => [
            'display_name' => 'Danh sách'
        ],
        'create' => [
            'display_name' => 'Thêm mới'
        ],
        'store' => [
            'display_name' => 'Lưu'
        ],
        'edit' => [
            'display_name' => 'Cập nhật'
        ],
        'update' => [
            'display_name' => 'Cập nhật'
        ],
        'destroy' => [
            'display_name' => 'Xóa'
        ],
        'bulkDestroy' => [
            'display_name' => 'Xóa nhiều'
        ],

    ];