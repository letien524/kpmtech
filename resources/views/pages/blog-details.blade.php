@extends('index')
@section('content')

    @include('components.banner-style',['banner'=>@$blog['banner']])

    <div class="sub full_container visual01">
        <div class="container">
            @include('section.header')

            @include('components.breadcrumb',['breadcrumb'=> $breadcrumb])

            <div class="container contents_warp">
                <div class="contents_side">
                    <div class="sub_contents design map">
                        <div class="location02">
                            <h1 class="title">{{$blog['name']}}</h1>
                        </div>

                       <div>
                           {!! $blog['content'] !!}
                       </div>
                        <hr>
                        @if(!empty($relatedBlogs))

                            <h2 class="title02">{{__('TIN TỨC LIÊN QUAN')}}</h2>

                            <div class="vk-blog-relate__list">
                                @foreach($relatedBlogs as $blog)
                                    <a href="{{route('blog.detail',$blog['slug'])}}" title="{{$blog['name']}}">{{$blog['name']}}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @include('components.go-to-top')
        </div>
    </div>

@endsection