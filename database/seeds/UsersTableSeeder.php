<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'superadmin',
                'fullname' => NULL,
                'image' => NULL,
                'email' => NULL,
                'password' => '$2y$10$.MKAgQw4XCAqnWBGB3ib8O5QsdPPcDTgpotyiap71WN0hvBvoQdQq',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'tung_admin',
                'fullname' => NULL,
                'image' => NULL,
                'email' => NULL,
                'password' => '$2y$10$fW9Gc8inOMMM3TSt4O47C.NlSX3dN5WhIQ/3XiFcorIbZciGoOwe2',
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}