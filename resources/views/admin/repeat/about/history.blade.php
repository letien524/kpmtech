<?php $id = isset($id) ? $id : (int) round(microtime(true) * 1000); ?>

<tr>
    <td><span class="index-history">{{request('index') ?: @$index}}</span></td>
    <td> <input type="text" class="form-control" name="content[history][{{$id}}][date]" value="{{@$val['date']}}"></td>
    <td style="position:relative">
        <textarea class="form-control" name="content[history][{{$id}}][content]" rows="5">{{@$val['content']}}</textarea>
        <a href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="repeat-btn-remove text-danger" title="Xóa"><i class="fa fa-minus"></i></a>
        <a href="javascript:void(0);" onclick="rowAdd(event,this,'{{route('repeat.about.history')}}','.index-history')" class="repeat-btn-add text-primary" title="Thêm"><i class="fa fa-plus"></i></a>

    </td>
</tr>
