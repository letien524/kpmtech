
<!DOCTYPE html>
<html lang="en"><head>
    <title> Example </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{getAsset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{getAsset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{getAsset('css/style.css')}}">
    <link rel="stylesheet" href="{{getAsset('css/reponsive.css')}}">
    <link rel="stylesheet" href="{{getAsset('css/swiper.min.css')}}">
</head>
<body >
<main>
    <section id="error">
        <div class="content-err">
            <div class="logo"><a href="{{route('home')}}"><img src="{{strlen(@$site_settings['logo_menu']) ? imageUrlRender($site_settings['logo_menu']) :  getAsset('images/logo2.png')}}" class="img-fluid" alt=""></a></div>
        </div>
        <div class="info-err">
            <div class="cont">
                <h1>{{__('PAGE NOT FOUND')}}</h1>
                <p>{{__('The link you clicked may be broken or the page may have been removed.')}}</p>
                <div class="back-home">
                    <a href="{{route('home')}}">{{__('BACK TO HOMEPAGE')}}</a>
                </div>
            </div>
        </div>
    </section>
</main>
<footer></footer>
</body>
<script type="text/javascript" src="{{getAsset('js/bootstrap.js')}}"></script>
<script src="{{getAsset('js/swiper.min.js')}}"></script>
<script type="text/javascript" src="{{getAsset('js/main.js')}}"></script>
<script type="text/javascript" src="{{getAsset('js/wow.js')}}"></script>
<script type="text/javascript" src="{{getAsset('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{getAsset('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js')}}"></script>
</html>