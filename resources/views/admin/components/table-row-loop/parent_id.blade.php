<td>
    @if(isCurrentRoute('productCategory.index'))
        <?php $categories = strlen($item->parent['name']) ? $item->parent['name'] : '__' ?>

        <a href="{{ route("{$module['model']}.edit",$item) }}" title="{{ $categories }}" class="text-default">
            {{$categories}}
        </a>
    @endif
</td>
