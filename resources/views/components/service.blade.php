<?php

    $link = route('service.detail', ['slug' => $service['slug']]);
    $name = $service[getColumnByLang('name')];
    $excerpt = $service[getColumnByLang('excerpt')];
    $image = imageUrlRender($service['image']);
    $number = @$loop->index % 2;

?>
@if(@$type == 'relatedService')
    <div class="col-md-4 wow fadeInUp wHighlight" data-wow-delay=".{{ (@$loop->index % 3) + 1 }}5s">
        <div class="item">
            <div class="avarta"><a href="{{$link}}"><img src="{{$image}}" width="100%" class="img-fluid" alt="{{$name}}"></a></div>
            <div class="info">
                <h3><a href="{{$link}}">{{$name}}</a></h3>
            </div>
        </div>
    </div>
@else

    @if($number == 0)
        <div class="row">
            <div class="col-md-7">
                <div class="left pull-left wow fadeInLeft wHighlight" data-wow-delay=".25s">
                    <div class="avarta"><img src="{{$image}}" class="img-fluid" width="100%" alt="{{$name}}"></div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="right pull-right wow fadeInRight wHighlight" data-wow-delay=".25s">
                    <div class="title">{{$name}}</div>
                    <p>{{$excerpt}}</p>
                    <div class="link-view">
                        <a href="{{$link}}">{{__('CHI TIẾT')}}</a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-5">
                <div class="right pull-left wow fadeInLeft wHighlight" data-wow-delay=".25s">
                    <div class="title">{{$name}}</div>
                    <p>{{$excerpt}}</p>
                    <div class="link-view">
                        <a href="{{$link}}">{{__('CHI TIẾT')}}</a>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="left pull-left wow fadeInLeft wHighlight" data-wow-delay=".25s">
                    <div class="avarta"><img src="{{$image}}" class="img-fluid" width="100%" alt="{{$name}}"></div>
                </div>
            </div>
        </div>
    @endif
@endif
