<?php
    $except = ['show'];
    return [
        'dashboard' => [
            'name' => 'dashboard',
            'except' => $except
        ],

        'user' => [
            'name' => 'user',
            'except' => $except
        ],

        'home' => [
            'name' => 'home',
            'except' => $except
        ],

        'blog' => [
            'name' => 'blog',
            'except' => $except
        ],

        'blogSetting' => [
            'name' => 'blogSetting',
            'except' => $except
        ],

        'product' => [
            'name' => 'product',
            'except' => $except
        ],

        'productCategory' => [
            'name' => 'productCategory',
            'except' => $except
        ],

        'about' => [
            'name' => 'about',
            'except' => $except
        ],

        'contact' => [
            'name' => 'contact',
            'except' => $except
        ],

        'contactSetting' => [
            'name' => 'contactSetting',
            'except' => $except
        ],

//        'menu' => [
//            'name' => 'menu',
//            'except' => $except
//        ],

        'settingGeneral' => [
            'name' => 'settingGeneral',
            'except' => $except
        ],

        'settingMail' => [
            'name' => 'settingMail',
            'except' => $except
        ],
    ];