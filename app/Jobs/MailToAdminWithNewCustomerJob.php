<?php

namespace App\Jobs;

use App\Contact;
use App\Mail\MailToAdminWithNewCustomer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class MailToAdminWithNewCustomerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, JobTrait;

    protected $customer;

    public function __construct(Contact $customer)
    {
        $this->initMailConfig();
        $this->customer = $customer;
    }


    public function handle()
    {
        $customer = $this->customer;

        $mail = getSettings('mail');

        $mailTo = filter_var(@$mail['to'],FILTER_VALIDATE_EMAIL) ? $mail['to'] : config('mail.username');

        Mail::to($mailTo)
            ->send(new MailToAdminWithNewCustomer($customer));
    }
}
