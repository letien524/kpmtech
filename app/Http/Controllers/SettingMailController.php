<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingMailController extends AdminController
{
    protected function fields(){

        return [];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'setting';

    protected function module(){
        return [
            'name' => 'Mail SMTP',
            'model' => 'settingMail',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Setting::firstOrCreate(['name'=>'mail']);
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }



    public function update(Request $request, Setting $settingMail)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $settingMail->update($input);

        return back()->with($this->flashMessages);
    }
}
