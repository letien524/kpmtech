<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover data-table">
        <thead>
        <tr>
            <th style="width: 30px;">
                <input type="checkbox" name="selectAll" onchange="inputSelectAll(this)">
            </th>
            <th style="width: 30px;">No.</th>
            @foreach($getIndexLayout as $key => $column)
                <?php //$column = is_array($column) ? $column['name'] : $column ?>
                <th>{{ $column }}</th>
            @endforeach
            <th style="width: 150px;text-align: center;">Thao tác</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $index => $item)
            <tr>
                <td><input type="checkbox" name="selectItem[]" value="{{ $item->id }}"></td>
                <td>{{ $index+1 }}</td>
                @foreach($getIndexLayout as $key => $column)
                    @if(view()->exists("admin.components.table-row-loop.$key"))
                        @include("admin.components.table-row-loop.$key")
                    @else
                        <td>{{ $item->$key }}</td>
                    @endif
                @endforeach
                <td class="text-center">
                    @if(isset($config['detail']))
                        <a href="{{ route($config['model'].".detail",['id'=>$item->slug]) }}" target="_blank" title="{{ __('Xem') }}"> <i class="fa fa-eye fa-fw"></i> {{ __('Xem') }}</a> |
                    @endif
                    <a href="{{ route($config['model'].".edit",['id'=>$item]) }}" title="{{ __('Sửa') }}"> <i class="fa fa-pencil fa-fw"></i> {{ __('Sửa') }}</a> |
                    <a href="{{ route($config['model'].".destroy",['id'=>$item]) }}"
                       onclick="itemDeleteConfirm(this,event)"
                       data-target="#detroyConfirm" data-toggle="modal" class="text-danger" title="{{ __('Xóa') }}">
                        <i class="fa fa-trash-o fa-fw"></i> {{ __('Xóa') }}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
