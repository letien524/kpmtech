<?php

namespace App\Providers;


use App\Contact;
use App\Setting;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    protected function seoInitConfig($site_settings){
        SEOMeta::setRobots(getSiteRobots(@$site_settings['index']));
        SEOMeta::setCanonical(url()->current());
    }

    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->init();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


        //Const
        $this->loadConst();

        //Helpers
        $this->loadHelpers();

    }
    protected function loadHelpers()
    {
        require_once app_path(). '/Helpers/helper.php';
    }

    protected function loadConst()
    {
        require_once app_path(). '/Const/const.php';
    }

    protected function init(){

        $site_settings = getSettings('general');

        $this->seoInitConfig($site_settings);


        View::composer('*',function($view) use($site_settings){

            $currentLang = app()->getLocale();
            $site_footer = getSettings('footer');
            $productCategories = getProductCategories();


            $data = [
                'site_settings' => $site_settings,
                'site_footer' => $site_footer,
                'currentLang' => $currentLang,
                'productCategories' => $productCategories,
            ];


            $view->with($data);
        });
    }
}
