<?php

    namespace App\Http\Controllers;

    use App\Http\Middleware\isDeveloper;
    use App\User;
    use Illuminate\Http\Request;

    class UserController extends AdminController
    {

        protected function fields(){
            /*^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$*/
            return [
                'name' => "required|unique:users,name|regex:/^(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/",
                'fullname' => "max:50",
                'password' => "required",
                're-password' => "required|same:password",
            ];
        }

        protected function messages(){
            return [
                'name.required' => 'Trường tài khoản không được bỏ trống',
                'name.unique' => 'Trường tài khoản đã có trong cơ sở dữ liệu.',
                'name.regex' => 'Trường tài khoản có định dạng không hợp lệ.',
                're-password.required' => 'Trường nhập lại mật khẩu không được bỏ trống',
                're-password.same' => 'Mật khẩu không trùng khớp',

            ];
        }

        protected $permissionDenied=[
            'flash_level' => 'error',
            'flash_message' => 'Permission Denied!',
        ];

        protected $model = 'user';

        protected function module(){
            return [
                'name' => 'Quản trị viên',
                'model' => 'user',
                'table' =>[
                    'name' => 'Tài khoản',
                    'fullname' => 'Họ tên',
                    'level' => 'Vai trò',
                ]
            ];
        }

        public function __construct()
        {
            parent::__construct();
            $this->middleware('isDeveloper',['except' => ['edit', 'update']]);
        }

        public function index()
        {
            if(isDeveloper()){
                $data['data'] = User::latest('created_at')->get();
            }else{
                $data['data'] = User::where('name','<>','superadmin')->latest('created_at')->get();
            }


            $data['module'] = $this->module();

            return view("admin.module.{$this->module()['model']}.index", $data);
        }


        public function create()
        {
            $data['module'] = $this->module();
            return view("admin.module.{$this->module()['model']}.create-edit",$data);
        }

        public function store(Request $request)
        {
            $this->validate($request, $this->fields(), $this->messages());


            if(isDeveloper()){
                $input = $request->all();
            }else{
                $input = $request->except(['level']);
            }

            $input['password'] = bcrypt( $input['password']);

            $data = User::create($input);

            return redirect()->route("{$this->module()['model']}.edit",$data)->with($this->flashMessages);

        }

        public function edit(User $user)
        {

            if(!isDeveloper()){
                if($user['name'] == 'superadmin'){
                    return back()->with($this->permissionDenied);
                }

                if($user['id'] != auth()->id() && !isAdmin()){
                    return back()->with($this->permissionDenied);
                }
            }


            $user['password'] = '';
            $data['data'] = $user;
            $data['module'] = array_merge($this->module(),[
                'action' => 'update'
            ]);
            return view("admin.module.{$this->module()['model']}.create-edit", $data);
        }


        public function update(Request $request, User $user)
        {

            if(!isDeveloper()){
                if($user['name'] == 'superadmin'){
                    return back()->with($this->permissionDenied);
                }

                if($user['id'] != auth()->id() && !isAdmin()){
                    return back()->with($this->permissionDenied);
                }
            }

            if(isDeveloper()){
                $input = $request->except('name');
            }else{
                $input = $request->except(['name','level']);
            }

            if(strlen(@$input['password'])){
                $this->validate($request, [
                    'fullname' => "required|max:50",
                    're-password' => 'required|same:password',
                ], $this->messages());

                $input['password'] = bcrypt( $input['password']);
            }
            else{
                $this->validate($request, [
                    'fullname' => "required|max:50",
                ], $this->messages());

                $input['password'] = $user['password'];
            }

            $user->update($input);

            return back()->with($this->flashMessages);
        }
    }
