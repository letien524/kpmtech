<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends AdminController
{

    protected function fields(){
        $id = $this->getIdOnUrl();

        return [
            'name' => "required|max:100|unique:products,name,{$id}",
            'slug' => "required|unique:products,slug,{$id}",
            'image' => "required",
        ];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'product';

    protected function module(){
        return [
            'name' => 'Sản phẩm',
            'model' => 'product',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'categories' => 'Danh mục',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Product::latest()->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();

        $data['categories'] = ProductCategory::orderBy('name','asc')->get();

        return view("admin.module.{$this->module()['model']}.create-edit",$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->except('categories');

        $data = Product::create($input);

        $data->categories()->sync($request->categories);

        return redirect()->route("{$this->module()['model']}.edit",$data)->with($this->flashMessages);

    }

    public function edit(Product $product)
    {
        $data['data'] = $product;

        $data['categories'] = ProductCategory::orderBy('name','asc')->get();

        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, Product $product)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->except('categories');

        $input['display'] = isset($input['display']);

        $product->update($input);

        $product->categories()->sync($request->categories);

        return back()->with($this->flashMessages);
    }

}