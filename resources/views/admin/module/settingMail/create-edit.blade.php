@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',isUpdate(@$module['action']) ? config('admin.method.edit.display_name') : config('admin.method.create.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{!! updateOrStoreRouteRender(@$module['action'],$module['model'],@$data) !!}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if(isUpdate(@$module['action']))
        {{method_field('put')}}
    @endif
    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1"data-toggle="tab">{!! config('admin.tab.general.display_name') !!}</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab-1">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Mail driver</label>
                                <input type="text" placeholder="smtp" class="form-control" name="content[driver]" value="{{old('content.driver',@$data['content']['driver'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail host</label>
                                <input type="text" placeholder="smtp.mailtrap.io" class="form-control" name="content[host]" value="{{old('content.host',@$data['content']['host'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail port</label>
                                <input type="text" placeholder="587" class="form-control" name="content[port]" value="{{old('content.port',@$data['content']['port'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail encryption</label>
                                <input type="text" placeholder="tls" class="form-control" name="content[encryption]" value="{{old('content.encryption',@$data['content']['encryption'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail username</label>
                                <input type="text" placeholder="hello@example.com" class="form-control" name="content[username]" value="{{old('content.username',@$data['content']['username'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail password</label>
                                <input type="password" placeholder="****" class="form-control" name="content[password]" value="{{old('content.password',@$data['content']['password'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail Name</label>
                                <input type="text" placeholder="Example" class="form-control" name="content[name]" value="{{old('content.name',@$data['content']['name'])}}">
                            </div>

                            <div class="form-group">
                                <label>Mail to</label>
                                <input type="text" placeholder="hello@example.com" class="form-control" name="content[to]" value="{{old('content.to',@$data['content']['to'])}}">
                            </div>

                        </div>

                    </div>
                </div> <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

        <div class="box box-solid">
            <div class="box-body">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
                @include("admin.components.save-as")
            </div>
        </div>
    </form>
@endsection