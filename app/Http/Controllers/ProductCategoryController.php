<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends AdminController
{

    protected function fields(){
        $id = $this->getIdOnUrl();

        return [
            'name' => "required|max:100|unique:product_categories,name,{$id}",
            'slug' => "required|unique:product_categories,slug,{$id}",
        ];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'productCategory';

    protected function module(){
        return [
            'name' => 'Danh mục sản phẩm',
            'model' => 'productCategory',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'parent_id' => 'Danh mục cha',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = ProductCategory::latest()->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        $data['categories'] = ProductCategory::where('parent_id',null)->orWhere('parent_id',0)->orderBy('name','asc')->get();
        return view("admin.module.{$this->module()['model']}.create-edit",$data);
    }

    public function store(Request $request)
    {

        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $data = ProductCategory::create($input);

        return redirect()->route("{$this->module()['model']}.edit",$data)->with($this->flashMessages);

    }

    public function edit(ProductCategory $productCategory)
    {
        $data['data'] = $productCategory;
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        $data['categories'] = ProductCategory::where('parent_id',null)->orWhere('parent_id',0)->orderBy('name','asc')->get();

        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, ProductCategory $productCategory)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $input['display'] = isset($input['display']);

        $productCategory->update($input);

        return back()->with($this->flashMessages);
    }

}
