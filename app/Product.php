<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use ModelTrait;

    protected $guarded = ['id','created_at','updated_at'];
    protected $casts = [
        'display' => 'bool',
    ];

    public function categories(){
        return $this->belongsToMany(ProductCategory::class,'category_product','product_id','category_id');
    }
}
