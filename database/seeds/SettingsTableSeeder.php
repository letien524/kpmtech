<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'general',
                'content' => '{"favicon":null,"logo":{"desktop":null,"mobile":null},"company_name":"C\\u00f4ng ty KPM TECH VINA","company_address":"<p>Nh&agrave; m&aacute;y 1: L&ocirc; CN 09-20 CCN Ninh Hi\\u1ec7p, qu\\u1ed1c l\\u1ed9 1A, X&atilde; Ninh Hi\\u1ec7p, huy\\u1ec7n Gia L&acirc;m, H&agrave; N\\u1ed9i<\\/p>\\r\\n\\r\\n<p>Nh&agrave; m&aacute;y 2: \\u0110\\u01b0\\u1eddng 6 L&ocirc; E KCN B&igrave;nh Xuy&ecirc;n 1, H\\u01b0\\u01a1ng Canh, V\\u0129nh Ph&uacute;c<\\/p>\\r\\n\\r\\n<p>Nh&agrave; m&aacute;y 3: Ng&atilde; 3 Nam Ti\\u1ebfn, Ph\\u1ed5 Y&ecirc;n, Th&aacute;i Nguy&ecirc;n<\\/p>","company_hotline":"<p>Team kinh doanh 1: 0965405843 &nbsp;Mr T&ugrave;ng<\\/p>\\r\\n\\r\\n<p>Team kinh doanh 2: 0988972665 &nbsp;Ms Ph\\u01b0\\u01a1ng<\\/p>","script_head":null,"script_foot":null,"meta_image":null,"index":true,"meta_title":null,"meta_description":null,"meta_title_en":null,"meta_description_en":null,"meta_title_ko":null,"meta_description_ko":null}',
                'created_at' => '2019-07-16 15:23:06',
                'updated_at' => '2019-08-24 11:58:04',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'footer',
                'content' => '{"social":{"1563266701705":{"icon":"fa-facebook-f","name":"facebook","link":"#"},"1563266702630":{"icon":"fa-twitter","name":"twitter","link":"#"},"1563266703469":{"icon":"fa-youtube-play","name":"youtube","link":"#"}},"news_register":{"vi":"<p>B\\u1eb1ng c&aacute;ch g\\u1eedi bi\\u1ec3u m\\u1eabu n&agrave;y, t&ocirc;i \\u0111\\u1ed3ng &yacute; \\u0111\\u1ec3 th&ocirc;ng tin c&aacute; nh&acirc;n v&agrave; th&ocirc;ng tin li&ecirc;n h\\u1ec7 c\\u1ee7a t&ocirc;i \\u0111\\u01b0\\u1ee3c x\\u1eed l&yacute; v&agrave; s\\u1eed d\\u1ee5ng cho m\\u1ee5c \\u0111&iacute;ch truy\\u1ec1n th&ocirc;ng ti\\u1ebfp th\\u1ecb. B\\u1ea1n c&oacute; th\\u1ec3 t&igrave;m th&ecirc;m th&ocirc;ng tin chi ti\\u1ebft v\\u1ec1 Ch&iacute;nh s&aacute;ch b\\u1ea3o m\\u1eadt c\\u1ee7a ch&uacute;ng t&ocirc;i t\\u1ea1i li&ecirc;n k\\u1ebft sau:&nbsp;<a href=\\"\\">Ch&iacute;nh s&aacute;ch b\\u1ea3o m\\u1eadt.<\\/a><\\/p>","en":"<p>ta B\\u1eb1ng c&aacute;ch g\\u1eedi bi\\u1ec3u m\\u1eabu n&agrave;y, t&ocirc;i \\u0111\\u1ed3ng &yacute; \\u0111\\u1ec3 th&ocirc;ng tin c&aacute; nh&acirc;n v&agrave; th&ocirc;ng tin li&ecirc;n h\\u1ec7 c\\u1ee7a t&ocirc;i \\u0111\\u01b0\\u1ee3c x\\u1eed l&yacute; v&agrave; s\\u1eed d\\u1ee5ng cho m\\u1ee5c \\u0111&iacute;ch truy\\u1ec1n th&ocirc;ng ti\\u1ebfp th\\u1ecb. B\\u1ea1n c&oacute; th\\u1ec3 t&igrave;m th&ecirc;m th&ocirc;ng tin chi ti\\u1ebft v\\u1ec1 Ch&iacute;nh s&aacute;ch b\\u1ea3o m\\u1eadt c\\u1ee7a ch&uacute;ng t&ocirc;i t\\u1ea1i li&ecirc;n k\\u1ebft sau:&nbsp;<a href=\\"\\">Ch&iacute;nh s&aacute;ch b\\u1ea3o m\\u1eadt.<\\/a><\\/p>","ko":"<p>th B\\u1eb1ng c&aacute;ch g\\u1eedi bi\\u1ec3u m\\u1eabu n&agrave;y, t&ocirc;i \\u0111\\u1ed3ng &yacute; \\u0111\\u1ec3 th&ocirc;ng tin c&aacute; nh&acirc;n v&agrave; th&ocirc;ng tin li&ecirc;n h\\u1ec7 c\\u1ee7a t&ocirc;i \\u0111\\u01b0\\u1ee3c x\\u1eed l&yacute; v&agrave; s\\u1eed d\\u1ee5ng cho m\\u1ee5c \\u0111&iacute;ch truy\\u1ec1n th&ocirc;ng ti\\u1ebfp th\\u1ecb. B\\u1ea1n c&oacute; th\\u1ec3 t&igrave;m th&ecirc;m th&ocirc;ng tin chi ti\\u1ebft v\\u1ec1 Ch&iacute;nh s&aacute;ch b\\u1ea3o m\\u1eadt c\\u1ee7a ch&uacute;ng t&ocirc;i t\\u1ea1i li&ecirc;n k\\u1ebft sau:&nbsp;<a href=\\"\\">Ch&iacute;nh s&aacute;ch b\\u1ea3o m\\u1eadt.<\\/a><\\/p>"},"map":"<iframe src=\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d3724.774849760311!2d105.82069491492912!3d21.001660494078806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135adc43ec75a99%3A0x6a7b4b6f36336faa!2zQ8O0bmcgdHkgY-G7lSBwaOG6p24gY8O0bmcgbmdo4buHIHbDoCB0cnV54buBbiB0aMO0bmcgR0NP!5e0!3m2!1svi!2s!4v1557113638754!5m2!1svi!2s\\" width=\\"100%\\" height=\\"270\\" frameborder=\\"0\\" style=\\"border:0\\" allowfullscreen><\\/iframe>"}',
                'created_at' => '2019-07-16 15:23:06',
                'updated_at' => '2019-07-16 15:46:33',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'mail',
                'content' => '{"driver":null,"host":null,"port":null,"encryption":null,"username":"mailcentermanagers@gmail.com","password":"ysywyqyzfcpivvpq","name":"KPM TECH VINA","to":"letien524@gmail.com"}',
                'created_at' => '2019-07-16 15:55:24',
                'updated_at' => '2019-08-24 22:32:59',
            ),
        ));
        
        
    }
}