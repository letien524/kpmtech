<?php
    define('__IMAGE_THUMBNAIL_DEFAULT', asset('placeholder.png'));

    define('__STORAGE_APP', '/storage/app');
    define('__STORAGE_PUBLIC', '/public');
    define('__STORAGE_IMAGE', __STORAGE_PUBLIC.'/images');
    define('__STORAGE_FILE', __STORAGE_PUBLIC.'/files');
