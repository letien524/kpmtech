<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use ModelTrait;

    protected $guarded = ['id','created_at','updated_at'];

    protected $casts = [
        'display' => 'bool',
    ];


}
