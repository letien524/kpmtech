<?php
    return [

        'dashboard' => [
            'name' => 'Bảng điều khiển',
            'icon' => 'fa-users',
            'route' => 'dashboard.index'

        ],

        'user' => [
            'name' => 'Quản trị viên',
            'icon' => 'fa-users',
            'route' => 'user.index',
            'role' => 'developer'

        ],

        'home' => [
            'name' => 'Trang chủ',
            'icon' => 'fa-newspaper-o',
            'route' => 'home.index',
        ],

        'blog' => [
            'name' => 'Tin tức',
            'icon' => 'fa-newspaper-o',
            'children' => [
                ['name' => 'Danh sách', 'route' => 'blog.index'],
                ['name' => 'Cấu hình', 'route' => 'blogSetting.index'],
            ]
        ],

        'product' => [
            'name' => 'Sản phẩm',
            'icon' => 'fa-newspaper-o',
            'children' => [
                ['name' => 'Danh sách', 'route' => 'product.index'],
                ['name' => 'Danh mục', 'route' => 'productCategory.index'],
            ]
        ],

        'about' => [
            'name' => 'Giới thiệu',
            'icon' => 'fa-newspaper-o',
            'route' => 'about.index',
        ],

        'contact' => [
            'name' => 'Liên hệ',
            'icon' => 'fa-newspaper-o',
            'children' => [
                ['name' => 'Danh sách liên hệ',  'route' => 'contact.index',],
                ['name' => 'Cấu hình', 'route' => 'contactSetting.index'],
            ]
        ],

        'setting' => [
            'name' => 'Cấu hình',
            'icon' => 'fa-cogs',
            'children' => [
                ['name' => 'Cấu hình chung', 'route' => 'settingGeneral.index'],
                ['name' => 'Mail SMTP', 'route' => 'settingMail.index'],
            ]
        ],

    ];