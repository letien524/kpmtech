function OasisHyeonCustrom_Popup(layer_idx,popup_url,popup_width,popup_height,popup_scroll)
{
	var obj = this;
	var layer_idx;
	
	this.setCookie = function(cName, cValue, cDay)
	{
		var expire = new Date();
		expire.setDate(expire.getDate() + cDay);
		cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
		if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
		document.cookie = cookies;
	}
	
	this.getCookie = function(cName)
	{
	  cName = cName + '=';
	  var cookieData = document.cookie;
	  var start = cookieData.indexOf(cName);
	  var cValue = '';
	  if(start != -1){
		   start += cName.length;
		   var end = cookieData.indexOf(';', start);
		   if(end == -1)end = cookieData.length;
		   cValue = cookieData.substring(start, end);
	  }
	  return unescape(cValue);
	}
	
	this.doc_ready = function()
	{
		var temp_cookie_name = 'oasis_hyeon_popup'+layer_idx;
		var is_cookie = obj.getCookie(temp_cookie_name);
		// 도큐먼트 래디가 되었을 때 쿠키 설정.
		
		if(is_cookie) // 지정된 이름의 쿠기가 있다면.
		{				 
		}
		else
		{
			window.open(popup_url,'_blank',"scrollbars=yes,width="+popup_width+",height="+popup_height); 
		}
	};
	
	this.close_click = function()
	{
		if(!document.getElementById('oasis_hyeon_close_button'+layer_idx))
		{
			return false;
		}
		if (document.getElementById('oasis_hyeon_close_button'+layer_idx).addEventListener) // ie8은 addEventListener가 없으므로...
		{
			document.getElementById('oasis_hyeon_close_button'+layer_idx).addEventListener('click',function()
			{
				var temp_cookie_name = 'oasis_hyeon_popup'+layer_idx;
				if(document.getElementById('oasis_hyeon_close'+layer_idx).checked == true) // 하루보기 체크 되어 있으면...
				{
					obj.setCookie(temp_cookie_name,temp_cookie_name,1);
					
				}
			},false);		
		}
		else
		{
			document.getElementById('oasis_hyeon_close_button'+layer_idx).attachEvent('onclick',function()
			{
				var temp_cookie_name = 'oasis_hyeon_popup'+layer_idx;
				if(document.getElementById('oasis_hyeon_close'+layer_idx).checked == true) // 하루보기 체크 되어 있으면...
				{
					obj.setCookie(temp_cookie_name,temp_cookie_name,1);
					window.open("about:blank","_self").close();
				}
				else
				{
					window.open("about:blank","_self").close();
				}
			});
		}
			
	}
	
	this.doc_ready();
	this.close_click();
}


