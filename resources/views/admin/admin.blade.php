<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <meta name="robots" content="noindex,nofollow">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @foreach(config('admin.style') as $style)
        <link rel="stylesheet" href="{{ asset($style) }}">
    @endforeach

    <script> function homeUrl() {
            return "{!! url('/') !!}";
        }</script>

    @foreach(config('admin.script') as $script)
        <script src="{{ asset($script) }}"></script>
    @endforeach

    @yield('script')

</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    @include('admin.section.header')

    @include('admin.section.sidebar')

    <div class="content-wrapper">

        @include('admin.section.content-header')

        <section class="content">

            @yield('content')

        </section>
    </div>

    @include('admin.section.footer')

    @include('admin.section.notify')
    @include('admin.section.modal')

</div>
@include('admin.section.ajax')
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</body>
</html>