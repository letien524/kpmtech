<section id="banner">
    <div class="content">
        <div class="avarta">
            <img src="{{strlen(@$bannerImage) ? imageUrlRender($bannerImage) : getAsset('images/bn5.png')}}" class="img-fluid" width="100%;" alt="">
        </div>
        <div class="caption-banner">
            <h1 class="{{ isCurrentRoute('home') ?: 'site-cate' }} wow fadeInUp wHighlight" data-wow-delay=".25s">{!! @$bannerName !!}</h1>
        </div>
    </div>
</section>