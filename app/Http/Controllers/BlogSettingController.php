<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class BlogSettingController extends AdminController
{
    protected function fields(){

        return [];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'page';

    protected function module(){
        return [
            'name' => 'Cấu hình tin tức',
            'model' => 'blogSetting',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Page::firstOrCreate(['type'=>'blog']);
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);


        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }



    public function update(Request $request, Page $blogSetting)
    {

        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $input['display'] = isset($input['display']);

        $blogSetting->update($input);

        return back()->with($this->flashMessages);
    }
}
