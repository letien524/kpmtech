<?php
    return [
        'general' => [
            'display_name' => 'Cấu hình chung'
        ],
        'info' => [
            'display_name' => 'Thông tin chung'
        ],
        'gallery' => [
            'display_name' => 'Thư viện ảnh'
        ],
        'seo' => [
            'display_name' => 'Cấu hình SEO'
        ],
        'vi' => [
            'display_name' => 'Tiếng Việt'
        ],
        'en' => [
            'display_name' => 'Tiếng Anh'
        ],
        'ko' => [
            'display_name' => 'Tiếng Hàn'
        ],

    ];