@extends('index')

@section('begin')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('end')
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endsection

@section('content')
    @include('components.banner-style',['banner'=>$product['banner']])

    <div class="sub full_container visual01">
        <div class="container">
            @include('section.header')

            @include('components.breadcrumb',['breadcrumb'=>$breadcrumb])

            <div class="container contents_warp">
                <div class="contents_side">
                    <div class="sub_contents design map">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="vk-shop-detail__top">
                                    <a  data-fancybox="gallery" href="{{$product['image']}}" title="{{$product['name']}}">
                                        <img src="{{$product['image']}}" alt="{{$product['name']}}" class="img-responsive">
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <h1 class="title">{{ $product['name'] }}</h1>

                               @if(strlen($product['excerpt']))
                                   <div class="vk-shop-detail__excerpt">
                                       {!! $product['excerpt'] !!}
                                   </div>
                                @endif

                              <div class="vk-shop-detail__contact"><a href="{{route('contact')}}" title="Liên hệ">Liên hệ</a></div>

                            </div>
                        
                        </div>


                        @if(strlen($product['content']))
                            <hr>
                            <div class="vk-shop-detail__bot">
                                <h2 class="title02 text-uppercase"> Thông tin sản phẩm </h2>

                                {!! $product['content'] !!}
                            </div>
                        @endif

                        @if($relatedProducts->count())
                            <hr>
                            <h2 class="title02 text-uppercase"> Sản phẩm liên quan </h2>
                            <div class="row vk-shop__list">
                                @foreach($relatedProducts as $product)
                                    @include('components.product',['$product' => $product])
                                @endforeach

                            </div>
                        @endif

                    </div>
                </div>
            </div>

            @include('components.go-to-top')

        </div>
    </div>

@endsection