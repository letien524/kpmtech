<?php

    namespace App;

    Trait ModelTrait
    {
        protected function isDisplay(){
            return $this->where('display',1);
        }

        protected function isFeature(){
            return $this->where('feature',1)->where('display',1);
        }

    }
