<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Page;
use Illuminate\Http\Request;

class ContactController extends AdminController
{
    protected function fields(){

        return [
           /* 'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',*/
        ];
        /*|regex:/^([0-9\s\-\+\(\)]*)$/|min:10*/
    }

    protected function messages(){
        return [];
    }

    protected $model = 'contact';

    protected function module(){
        return [
            'name' => 'Địa chỉ',
            'model' => 'contact',
            'table' =>[
                'name' => 'Tiêu đề',
                'email' => 'Email',
                'phone' => 'SĐT',
                'address' => 'Địa chỉ',
                'status' => 'Trạng thái',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Contact::latest()->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        return view("admin.module.{$this->module()['model']}.create-edit",$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $data = Contact::create($input);

        return redirect()->route("{$this->module()['model']}.edit",$data)->with($this->flashMessages);

    }

    public function edit(Contact $Contact)
    {
        $data['data'] = $Contact;
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }



    public function update(Request $request, Contact $contact)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $input['status'] = isset($input['status']);

        $contact->update($input);

        return back()->with($this->flashMessages);
    }
}
