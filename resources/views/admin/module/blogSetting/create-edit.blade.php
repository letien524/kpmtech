@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',isUpdate(@$module['action']) ? config('admin.method.edit.display_name') : config('admin.method.create.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{!! updateOrStoreRouteRender(@$module['action'],$module['model'],@$data) !!}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if(isUpdate(@$module['action']))
        {{method_field('put')}}
    @endif
    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1"data-toggle="tab">{!! config('admin.tab.general.display_name') !!}</a></li>
                <li class=""><a href="#tab-4"data-toggle="tab">{!! config('admin.tab.seo.display_name') !!}</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab-1">
                    <div class="row">
                       <div class="col-lg-4">
                           <div class="form-group">
                               <label>Banner</label>
                               <div class="image">
                                   <?php $value = old('banner',@$data['banner']); ?>
                                   <div class="image__thumbnail">
                                       <img src="{{ imageUrlRender($value) }}"  data-init="{!! __IMAGE_THUMBNAIL_DEFAULT !!}">
                                       <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                       <input type="hidden" value="{{ $value }}" name="banner"  />
                                       <div class="image__button" onclick="fileSelect(this)"><i class="fa fa-upload"></i> Upload</div>
                                   </div>
                               </div>
                           </div>
                       </div>

                    </div>
                </div> <!-- /.tab-pane -->

                <div class="tab-pane " id="tab-4">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta image</label>
                                <div class="image">
                                    <?php $value = old('meta_image',@$data['meta_image']); ?>
                                    <div class="image__thumbnail">
                                        <img src="{{ imageUrlRender($value) }}"  data-init="{!! __IMAGE_THUMBNAIL_DEFAULT !!}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" value="{{ $value }}" name="meta_image"  />
                                        <div class="image__button" onclick="fileSelect(this)"><i class="fa fa-upload"></i> Upload</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{{ old('meta_title', @$data['meta_title']) }}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description', @$data['meta_description']) !!}</textarea>
                            </div>
                        </div>

                    </div>
                </div> <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

        <div class="box box-solid">
            <div class="box-body">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
                @include("admin.components.save-as")
            </div>
        </div>
    </form>
@endsection