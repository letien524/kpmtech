<div class="container location">
    <ol class="location">
        <li><a href="{{route('home')}}">{{__('Trang chủ')}}</a></li>
        @foreach($breadcrumb as $item)
            <li><a href="{{$item['link']}}" title="{{$item['name']}}">{{$item['name']}}</a></li>
        @endforeach

    </ol>
</div>