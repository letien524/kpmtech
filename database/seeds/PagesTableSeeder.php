<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => NULL,
                'content' => '{"category":[{"name":"Surface treatment chemicals","link":"#"},{"name":"Technology center","link":"#"},{"name":"Fully automatic plating equipment","link":"#"},{"name":"EVER-ER","link":"#"}]}',
                'image' => NULL,
                'banner' => '{"desktop":null,"mobile":null}',
                'type' => 'home',
                'display' => NULL,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 11:14:32',
                'updated_at' => '2019-08-24 20:59:59',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => NULL,
                'content' => NULL,
                'image' => NULL,
                'banner' => NULL,
                'type' => 'blog',
                'display' => 0,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 11:14:53',
                'updated_at' => '2019-08-24 21:20:14',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => NULL,
                'content' => '{"map":"<iframe src=\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m14!1m8!1m3!1d7436.379595666088!2d105.662695!3d21.2639534!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134fb134f97e4fb%3A0x834fff30cfa598a1!2sKPM%20TECH%20VINA!5e0!3m2!1sen!2s!4v1566654965475!5m2!1sen!2s\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0;\\" allowfullscreen=\\"\\"><\\/iframe>"}',
                'image' => NULL,
                'banner' => NULL,
                'type' => 'contact',
                'display' => 0,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 11:49:46',
                'updated_at' => '2019-08-24 20:56:32',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => NULL,
                'content' => '{"management_philosophy":{"1566622906150":{"name":"Gi\\u00e1 tr\\u1ecb con ng\\u01b0\\u1eddi","content":"D\\u1ef1a tr\\u00ean s\\u1ef1 t\\u00f4n tr\\u1ecdng gi\\u00e1 tr\\u1ecb con ng\\u01b0\\u1eddi, v\\u0103n h\\u00f3a doanh nghi\\u1ec7p g\\u00f3p ph\\u1ea7n \\u0111\\u1ecbnh h\\u01b0\\u1edbng, ph\\u00e1t huy ti\\u1ec1m n\\u0103ng c\\u1ee7a m\\u1ed7i nh\\u00e2n vi\\u00ean."},"1566622908085":{"name":"S\\u1ef1 ph\\u00e1t tri\\u1ec3n c\\u00f4ng ngh\\u1ec7","content":"S\\u1ef1 ph\\u00e1t tri\\u1ec3n c\\u00f4ng ngh\\u1ec7 cao v\\u00e0 \\u0111\\u01b0\\u1ee3c c\\u00e1c kh\\u00e1ch h\\u00e0ng, \\u0111\\u1ed1i t\\u00e1c tin t\\u01b0\\u1edfng"},"1566622909579":{"name":"S\\u1ef1 h\\u00e0i l\\u00f2ng c\\u1ee7a kh\\u00e1ch h\\u00e0ng","content":"V\\u1edbi kh\\u1ea9u hi\\u1ec7u \\u201cLu\\u1ed1n \\u0111\\u1ea1t \\u0111\\u01b0\\u1ee3c s\\u1ef1 h\\u00e0i l\\u00f2ng c\\u1ee7a kh\\u00e1ch h\\u00e0ng th\\u00f4ng qua c\\u00e1c s\\u1ea3n ph\\u1ea9m kh\\u00f4ng khuy\\u1ebft t\\u1eadt v\\u00e0 d\\u1ecbch v\\u1ee5 t\\u1ed1i \\u01b0u\\u201d, ch\\u00fang t\\u00f4i s\\u1ebd mang l\\u1ea1i ni\\u1ec1m tin v\\u00e0 \\u1ea5n t\\u01b0\\u1ee3ng trong ng\\u00e0nh x\\u1eed l\\u00fd b\\u1ec1 m\\u1eb7t th\\u1ebf k\\u1ef7 21"},"1566622911406":{"name":"S\\u1ea3n ph\\u1ea9m th\\u00e2n thi\\u1ec7n v\\u1edbi m\\u00f4i tr\\u01b0\\u1eddng","content":"L\\u1ee3i th\\u1ebf c\\u1ea1nh tranh k\\u1ef9 thu\\u1eadt b\\u1eb1ng c\\u00e1ch ph\\u00e1t tri\\u1ec3n c\\u00e1c s\\u1ea3n ph\\u1ea9m kh\\u00f4ng c\\u00f3 ch\\u1ea5t \\u0111i\\u1ec1u ti\\u1ebft m\\u00f4i tr\\u01b0\\u1eddng."}},"history":{"1566622982653":{"date":"10\\/2017","content":"Th\\u00e0nh l\\u1eadp nh\\u00e0 m\\u00e1y \\u0111\\u1ea7u ti\\u00ean t\\u1ea1i Vi\\u1ec7t Nam, tr\\u1ee5 s\\u1edf t\\u1ea1i l\\u00f4 CN 09-20 C\\u1ee5m c\\u00f4ng nghi\\u1ec7p Ninh Hi\\u1ec7p, huy\\u1ec7n Gia L\\u00e2m, Th\\u00e0nh ph\\u1ed1 H\\u00e0 N\\u1ed9i. KPM TECH VINA nh\\u1eadp kh\\u1ea9u v\\u00e0 ph\\u00e2n ph\\u1ed1i h\\u00f3a ch\\u1ea5t H\\u00e0n Qu\\u1ed1c nh\\u1eb1m ph\\u1ee5c v\\u1ee5 nhu c\\u1ea7u th\\u1ecb tr\\u01b0\\u1eddng ng\\u00e0nh m\\u1ea1 t\\u1ea1i Vi\\u1ec7t Nam."},"1566622984505":{"date":"05\\/2018","content":"X\\u00e2y d\\u1ef1ng nh\\u00e0 m\\u00e1y s\\u1ea3n xu\\u1ea5t v\\u00e0 l\\u1eafp r\\u00e1p d\\u00e2y chuy\\u1ec1n anodizing v\\u1ecf \\u0111i\\u1ec7n tho\\u1ea1i t\\u1ea1i Ph\\u1ed5 Y\\u00ean, Th\\u00e1i Nguy\\u00ean . KPM TECH tr\\u1edf th\\u00e0nh \\u0111\\u1ed1i t\\u00e1c ch\\u00ednh c\\u1ee7a Samsung Th\\u00e1i Nguy\\u00ean v\\u1edbi vi\\u1ec7c v\\u1eadn h\\u00e0nh v\\u00e0 b\\u1ea3o tr\\u00ec 13 d\\u00e2y chuy\\u1ec1n anodizing t\\u1ea1i Samsung Th\\u00e1i Nguy\\u00ean"},"1566622989954":{"date":"11\\/2018","content":"Th\\u00e0nh l\\u1eadp chi nh\\u00e1nh t\\u1ea1i Ph\\u1ed5 Y\\u00ean, Th\\u00e1i Nguy\\u00ean c\\u1ee7a Korinn hotel \\u2013 h\\u1ec7 th\\u1ed1ng t\\u1ed5 h\\u1ee3p kh\\u00e1ch s\\u1ea1n 5 sao, nh\\u00e0 h\\u00e0ng, caf\\u00e9 Korinn ho\\u1ea1t \\u0111\\u1ed9ng nh\\u1eb1m m\\u1ee5c \\u0111\\u00edch h\\u1ed7 tr\\u1ee3 c\\u00e1c chuy\\u00ean gia H\\u00e0n Qu\\u1ed1c c\\u00f4ng t\\u00e1c ho\\u1ea1t \\u0111\\u1ed9ng d\\u00e0i ng\\u00e0y t\\u1ea1i Samsung."},"1566623004825":{"date":"6\\/2019","content":"Quy\\u1ebft \\u0111inh th\\u00e0nh l\\u1eadp nh\\u00e0 m\\u00e1y s\\u1ea3n xu\\u1ea5t linh ki\\u1ec7n \\u0111i\\u1ec7n t\\u1eed \\u0111\\u1ea7u ti\\u00ean t\\u1ea1i \\u0110\\u01b0\\u1eddng 6, L\\u00f4 E, KCN B\\u00ecnh Xuy\\u00ean 1, T\\u1ec9nh V\\u0129nh Ph\\u00fac. Nh\\u00e0 m\\u00e1y KPM TECH hi\\u1ec7n \\u0111ang l\\u00e0 \\u0111\\u1ed1i t\\u00e1c l\\u1edbn c\\u1ee7a c\\u00e1c c\\u00f4ng ty s\\u1ea3n xu\\u1ea5t linh ki\\u1ec7n \\u0111i\\u1ec7n t\\u1eed h\\u00e0ng \\u0111\\u1ea7u Vi\\u1ec7t Nam"},"1566623006418":{"date":"8\\/2019","content":"Ph\\u00e1t tri\\u1ec3n th\\u1ecb tr\\u01b0\\u1eddng, t\\u1eadp trung v\\u00e0o ng\\u00e0nh gia c\\u00f4ng x\\u1eed l\\u00fd b\\u1ec1 m\\u1eb7t m\\u1ea1 v\\u00e0ng linh ki\\u1ec7n \\u0111i\\u1ec7n t\\u1eed v\\u00e0 cung c\\u1ea5p c\\u00e1c h\\u00f3a ch\\u1ea5t, nguy\\u00ean v\\u1eadt li\\u1ec7u trong l\\u0129nh v\\u1ef1c m\\u1ea1 v\\u00e0ng b\\u1ec1 m\\u1eb7t linh ki\\u1ec7n \\u0111i\\u1ec7n t\\u1eed."}},"organization_structure":"<p style=\\"text-align: center;\\"><img src=\\"\\/uploads\\/files\\/image-20190824120351-1.png\\" \\/><span style=\\"font-size:12.0pt\\"><span style=\\"font-family:&quot;Times New Roman&quot;,serif\\"> <\\/span><\\/span><\\/p>"}',
                'image' => NULL,
                'banner' => '{"desktop":null,"mobile":null}',
                'type' => 'about',
                'display' => NULL,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 12:01:29',
                'updated_at' => '2019-08-24 12:04:38',
            ),
        ));
        
        
    }
}