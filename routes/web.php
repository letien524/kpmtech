<?php



    Route::prefix('admin')->group(function () {

        $models = config('admin.model');

        $repeaters = config('admin.repeater');

        //repeater
        if(count($repeaters)){
            Route::prefix('repeat')->middleware('auth')->group(function() use($repeaters){
                foreach ($repeaters as $module => $repeater){
                    foreach ($repeater as $value){
                        Route::get("{$module}-{$value}", function() use ($module,$value){
                            return view("admin.repeat.{$module}.{$value}");
                        })->name("repeat.{$module}.{$value}");
                    }

                }
            });

        }

        Route::get('', 'DashboardController@index')->name('admin.dashboard');
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//        Route::get('migrate', function(){
//            Artisan::call('migrate:refresh',['--seed' => true]);
//            return 'done';
//        } );



        foreach ($models as $model) {
            $controller = ucfirst($model['name']) . "Controller";
            Route::resource($model['name'], $controller, ['except' => $model['except']]);
            if (!in_array("bulkDestroy", $model['except'])) {
                Route::delete($model['name'], "$controller@bulkDestroy")->name($model['name'] . ".bulkDestroy");
                Route::put($model['name']."/{id}/ajax-display", "$controller@ajaxDisplay")->name($model['name'] . ".ajaxDisplay");
                Route::put($model['name']."/{id}/ajax-feature", "$controller@ajaxFeature")->name($model['name'] . ".ajaxFeature");
            }
        }
    });
    //site


        Route::get('lang/{lang}','SiteController@changeLang')->name('lang');

        Route::get('','SiteController@home')->name('home');

        Route::get('danh-muc-san-pham/{slug}','SiteController@productCategory')->name('product.category');
        Route::get('san-pham/{slug}','SiteController@productDetail')->name('product.detail');

        Route::get('tin-tuc','SiteController@blogs')->name('blog');
        Route::get('tin-tuc/{slug}','SiteController@blogDetails')->name('blog.detail');

        Route::get('lien-he','SiteController@contact')->name('contact');
        Route::post('lien-he','SiteController@contactSubmit');

        Route::get('gioi-thieu','SiteController@about')->name('about');


