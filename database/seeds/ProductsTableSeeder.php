<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'CaCl2 - Canxi clorua',
                'slug' => 'cacl2-canxi-clorua',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:06:45',
                'updated_at' => '2019-08-24 23:06:45',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Hóa chất ScharLau',
                'slug' => 'hoa-chat-scharlau',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:07:47',
                'updated_at' => '2019-08-24 23:07:47',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Inositol',
                'slug' => 'inositol',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:08:05',
                'updated_at' => '2019-08-24 23:08:05',
            ),
            3 => 
            array (
                'id' => 4,
            'name' => 'Iodine 99% (Hạt)',
                'slug' => 'iodine-99-hat',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:08:41',
                'updated_at' => '2019-08-24 23:08:41',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Naphthyl acetic acid - NAA',
                'slug' => 'naphthyl-acetic-acid-naa',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:09:06',
                'updated_at' => '2019-08-24 23:09:06',
            ),
            5 => 
            array (
                'id' => 6,
            'name' => 'Nhôm Hydroxit - Al(OH)3',
                'slug' => 'nhom-hydroxit-aloh3',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:09:21',
                'updated_at' => '2019-08-24 23:09:21',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Saponin',
                'slug' => 'saponin',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong> <br />
Công thức hóa học: CaCl2<br />
khối lượng phân tử 110,99 chất tinh thể màu trắng , có tính hút ẩm mạnh. Nhiệt độ nóng chảy 772 - 782oC nhiệt độ sôi > 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch bão hòa sôi ở 180oC . Trong các dung dịch có nồng độ khác nhau thì nhiệt độ sôi , nhiệt độ đông đặc thay đổi . <br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi hòa tan trong nước tan rất tốt kèm theo toả nhiều nhiệt dung dịch có vị mặn đắng <br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan dùng cho điện phân sản xuất canxi kim loại và điều chế các hợp kim của canxi<br />
- Với tính hút ẩm lớn của canxiclorua cho phép dùng nó làm tác nhân sấy khí và các chất lỏng .<br />
- Nhiệt độ đông đặc thấp của các dung dịch CaCl2 cho phép chúng làm chất tải lạnh trong các hệ thống lạnh... Để giảm bớt tính ăn mòn của CaCl2 thường bổ xung <a href="http://hoachatjsc.com/p/6077/na2co3">xôđa</a>, sữa vôi và các Crômát, Bicromát .<br />
- Do Áp suất hơi thấp của các hyđrát và các dung dịch nước Canxiclorua nên được dùng để hạn chế bụi đường xá.<br />
- Canxiclorua còn dùng để diệt cỏ trên đường sắt , chất keo tụ trong hóa dược và dược phẩm .<br />
Được dùng rất nhiều trong công việc khoan dầu khí .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:09:34',
                'updated_at' => '2019-08-24 23:09:34',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Silica',
                'slug' => 'silica',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:09:49',
                'updated_at' => '2019-08-24 23:09:49',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Tủ sấy',
                'slug' => 'tu-say',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:10:03',
                'updated_at' => '2019-08-24 23:10:03',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => '1- Bromonaphthalene',
                'slug' => '1-bromonaphthalene',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:10:17',
                'updated_at' => '2019-08-24 23:10:17',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => '1- Cyanoguanidine',
                'slug' => '1-cyanoguanidine',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:10:32',
                'updated_at' => '2019-08-24 23:10:32',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => '1- Naphthol',
                'slug' => '1-naphthol',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:10:46',
                'updated_at' => '2019-08-24 23:10:46',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => '3MPA',
                'slug' => '3mpa',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:11:00',
                'updated_at' => '2019-08-24 23:11:00',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => '7- Hydroxycoumarin',
                'slug' => '7-hydroxycoumarin',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:11:13',
                'updated_at' => '2019-08-24 23:11:13',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Acetone - C3H6O2',
                'slug' => 'acetone-c3h6o2',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:11:27',
                'updated_at' => '2019-08-24 23:11:27',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Acetyl Chloride',
                'slug' => 'acetyl-chloride',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:11:40',
                'updated_at' => '2019-08-24 23:11:40',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Acid acetic - CH3COOH',
                'slug' => 'acid-acetic-ch3cooh',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'excerpt' => '<p>Quy c&aacute;ch: 25kg/bao</p>

<p>Nh&agrave; sản xuất::&nbsp;<a href="#">Trung Quốc</a></p>',
                'gallery' => NULL,
                'content' => '<p><strong>1.Giới thiệu về sản phẩm :</strong>&nbsp;<br />
C&ocirc;ng thức h&oacute;a học: CaCl2<br />
khối lượng ph&acirc;n tử 110,99 chất tinh thể m&agrave;u trắng , c&oacute; t&iacute;nh h&uacute;t ẩm mạnh. Nhiệt độ n&oacute;ng chảy 772 - 782oC nhiệt độ s&ocirc;i &gt; 1600oC tỷ trọng 2152 - 2512 kg/m3 . Canxiclorua tan nhiều trong nước , dung dịch b&atilde;o h&ograve;a s&ocirc;i ở 180oC . Trong c&aacute;c dung dịch c&oacute; nồng độ kh&aacute;c nhau th&igrave; nhiệt độ s&ocirc;i , nhiệt độ đ&ocirc;ng đặc thay đổi .&nbsp;<br />
Bột Canxiclorua khan thu được khi phun sấy ở nhiệt độ cao hơn 260oC . Khi h&ograve;a tan trong nước tan rất tốt k&egrave;m theo toả nhiều nhiệt dung dịch c&oacute; vị mặn đắng&nbsp;<br />
<strong>2.Ứng dụng :</strong><br />
- Canxiclorua khan d&ugrave;ng cho điện ph&acirc;n sản xuất canxi kim loại v&agrave; điều chế c&aacute;c hợp kim của canxi<br />
- Với t&iacute;nh h&uacute;t ẩm lớn của canxiclorua cho ph&eacute;p d&ugrave;ng n&oacute; l&agrave;m t&aacute;c nh&acirc;n sấy kh&iacute; v&agrave; c&aacute;c chất lỏng .<br />
- Nhiệt độ đ&ocirc;ng đặc thấp của c&aacute;c dung dịch CaCl2 cho ph&eacute;p ch&uacute;ng l&agrave;m chất tải lạnh trong c&aacute;c hệ thống lạnh... Để giảm bớt t&iacute;nh ăn m&ograve;n của CaCl2 thường bổ xung&nbsp;<a href="http://hoachatjsc.com/p/6077/na2co3">x&ocirc;đa</a>, sữa v&ocirc;i v&agrave; c&aacute;c Cr&ocirc;m&aacute;t, Bicrom&aacute;t .<br />
- Do &Aacute;p suất hơi thấp của c&aacute;c hyđr&aacute;t v&agrave; c&aacute;c dung dịch nước Canxiclorua n&ecirc;n được d&ugrave;ng để hạn chế bụi đường x&aacute;.<br />
- Canxiclorua c&ograve;n d&ugrave;ng để diệt cỏ tr&ecirc;n đường sắt , chất keo tụ trong h&oacute;a dược v&agrave; dược phẩm .<br />
Được d&ugrave;ng rất nhiều trong c&ocirc;ng việc khoan dầu kh&iacute; .</p>',
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 23:11:58',
                'updated_at' => '2019-08-24 23:11:58',
            ),
        ));
        
        
    }
}