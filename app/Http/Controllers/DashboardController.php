<?php

namespace App\Http\Controllers;


use App\Blog;
use App\Contact;
use App\Product;

class DashboardController extends AdminController
{

    protected $model = 'blog';

    protected function module(){
        return [
            'name' => 'Bảng điều khiển',
            'model' => 'dashboard',
            'table' =>[
                'name' => 'Tiêu đề',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {

        $products = Product::count();
        $shownProducts = Product::isDisplay()->count();
//
        $contacts = Contact::count();
        $newContacts = Contact::where('status', '<>', 1)->orWhere('status',null)->count();
//
//
        $blogs  = Blog::count();
        $shownBlogs  = Blog::isDisplay()->count();


        $data = [
            'products' => $products,
            'shownProducts' => $shownProducts,

            'contacts' => $contacts,
            'newContacts' => $newContacts,

            'blogs' => $blogs,
            'shownBlogs' => $shownBlogs,


        ];

        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);

    }


}
