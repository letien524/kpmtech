<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use ModelTrait;

    protected $guarded = ['id','created_at','updated_at'];
    protected $casts = [
        'display' => 'bool',
        'is_menu' => 'bool',
    ];

    public function products(){
        return $this->belongsToMany(Product::class,'category_product','category_id','product_id');
    }

    public function children(){
        return $this->hasMany(ProductCategory::class,'parent_id');
    }

    public function parent(){
        return $this->belongsTo(ProductCategory::class,'parent_id');
    }

    public function hasProducts(){
        return $this->products()->count() ? true : false;
    }

}
