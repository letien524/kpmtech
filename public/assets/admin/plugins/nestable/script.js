$(function(){
    var menuContainer = $('#nestable'),
        menuContainerOutput = $('#nestable-output');

    window.updateOutput = function(e){
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    window.itemMenuEdit = function(el){
        console.log("itemMenuEdit");
        var menuEditForm = $('#menuEditForm');
        var input = menuEditForm.find('input');

        var item = $(el).closest('li');
        var attr = item.data();

        menuEditForm.data('id',attr['id']);
        input.val('');

        for (var key in attr) {
            input.each(function(){
                if($(this).attr('name') === key){
                    $(this).val(attr[key]);
                }
            })
        }
    }

    window.itemMenuSave = function(el){
        console.log("itemMenuSave");
        var menuEditForm = $(el);
        var modal = menuEditForm.closest('.modal');
        var id = menuEditForm.data('id');
        var input = menuEditForm.find('input');

        var item = menuContainer.find('li[data-id='+id+']');
        var itemLabel = item.find('.dd-handle').first();
        var linkLabel = item.find('.dd-action a span').first();

        var value = [];
        input.each(function(e){
            console.log($(this).data());
            var inputName = $(this).attr('name');
            var inputValue = $(this).val();
            value[inputName] = inputValue;

        });

        for (var key in value) {
            item.data(key,value[key]);
        }

        itemLabel.text(value['name']);
        var linkUrl = value['link'];
        if(linkUrl.length > 30){
            linkLabel.text(linkUrl.substr(0,30)+'...');
        }



        modal.modal('hide');

        updateOutput(menuContainer.data('output', menuContainerOutput));
    }

    window.itemMenuDelete = function(el){
        console.log("itemMenuDelete");
        var check = confirm('Bạn có chắc muốn xóa đi không?');
        if(check){
            $(el).closest('li').remove();
            updateOutput(menuContainer.data('output', menuContainerOutput));
        }

    }


    menuContainer.nestable().on('change', updateOutput);

    updateOutput(menuContainer.data('output', menuContainerOutput));

    var menuList = menuContainer.find('.dd-list').first();

    $('#menuAddBtn').on('click', function () {

        var form = $(this).closest('.menu-form');
        var optionSelected = form.find('option:selected');
        var selectTag = form.find('select');

        var customForm = form.find('.custom-link').first();

        var customLinkName = customForm.find('#customLinkName').val();
        var customLinkLink = customForm.find('#customLinkLink').val();

        var result = '';

        optionSelected.each(function (e) {
            var link = $(this).data('link');
            var name = $(this).text();
            result += '<li class="dd-item" data-id="' + (Date.now() + e) + '" data-link="' + link + '" data-name="' + name + '">' +
                '<div class="dd-handle">' + name + '</div>' +
                '<div class="dd-action">' +
                '<a href="#" data-target="#modal-default" data-toggle="modal" title="Sửa"  onclick="itemMenuEdit(this)" class="text-primary"><i class="fa fa-pencil-square-o"></i></a> | ' +
                '<a href="javascript:void(0)" title="Xóa" onclick="itemMenuDelete(this)" class="text-danger"><i class="fa fa-trash-o"></i></a>' +
                '</div>'
                '</li>';
        });


        if(customLinkName.length && customLinkLink.length){
            result += '<li class="dd-item" data-id="' + (Date.now() + optionSelected.length) + '" data-link="' + customLinkLink + '" data-name="' + customLinkName + '">' +
                '<div class="dd-handle">' + customLinkName + '</div>' +
                '<div class="dd-action">' +
                '<a href="#" data-target="#modal-default" data-toggle="modal" title="Sửa"  onclick="itemMenuEdit(this)" class="text-primary"><i class="fa fa-pencil-square-o"></i></a> | ' +
                '<a href="javascript:void(0)" title="Xóa" onclick="itemMenuDelete(this)" class="text-danger"><i class="fa fa-trash-o"></i></a>' +
                '</div>'
            '</li>';
        }

        menuList.append(result);

        selectTag.val(null).trigger('change');

        customForm.find('input').val('');

        updateOutput(menuContainer.data('output', menuContainerOutput));

    });



});
