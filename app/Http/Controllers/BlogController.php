<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends AdminController
{

    protected function fields(){
        $id = $this->getIdOnUrl();

        return [
            'name' => "required|max:100|unique:blogs,name,{$id}",
            'slug' => "required|unique:blogs,slug,{$id}",
            'image' => "required",
        ];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'blog';

    protected function module(){
        return [
            'name' => 'Tin tức',
            'model' => 'blog',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Blog::latest()->get();
        $data['module'] = $this->module();

        return view("admin.module.{$this->module()['model']}.index", $data);
    }


    public function create()
    {
        $data['module'] = $this->module();
        return view("admin.module.{$this->module()['model']}.create-edit",$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $data = Blog::create($input);

        return redirect()->route("{$this->module()['model']}.edit",$data)->with($this->flashMessages);

    }

    public function edit(Blog $blog)
    {
        $data['data'] = $blog;
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }


    public function update(Request $request, Blog $blog)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $input['display'] = isset($input['display']);

        $blog->update($input);

        return back()->with($this->flashMessages);
    }

}
