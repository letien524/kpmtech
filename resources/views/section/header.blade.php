<button class="gnb_button" onclick="DepthMenuControl('','mobile',$(this));">{{__('MENU')}}</button>

<div class="gnb gnb_serise">
    <div class="opacity">
    </div>
    <nav>
        <ul>
            <li class="first"><a href="{{route('home')}}"></a></li>
            <li><a class='depth_a01' href="{{route('home')}}">{{__('Trang chủ')}}</a></li>
            <li><a class='depth_a01' href="{{route('about')}}">{{__('Thông tin công ty')}}</a></li>
            <?php $count=0  ?>
            @foreach($productCategories as $category)
                @if($category->children->count())
                    <li>
                        <a class='depth_a01' href="javascript:DepthMenuControl({{$count}},'depth01','.depth_a01');void(0);">{{$category['name']}}</a>
                    </li>
                    <?php $count++ ?>
                @else
                    @if($category->products->count())
                        <li><a class='depth_a01' href="{{route('product.category',$category['slug'])}}">{{ $category['name'] }}</a></li>
                        <?php $count++ ?>
                    @endif
                @endif

            @endforeach
            <li class="copyright">
                <div class="vk-cpr">
                    <div class="vk-cpr__name">{{ @$site_settings['company_name'] }}</div>
                    <div class="vk-cpr__addr">{!! @$site_settings['company_address'] !!}</div>
                    <div class="vk-cpr__tel">
                        {!! @$site_settings['company_hotline'] !!}
                    </div>
                </div>
            </li>
        </ul>
    </nav>
</div>
<?php $count=0  ?>
@foreach(@$productCategories as $category)

    @if($category->children->count())
        <style>
            @media (min-width: 1171px){
                .depth02_gnb.depth02_{{$count}} {
                    top: calc(238px + {{ $count * 59 }}px);
                }
            }
        </style>
        <div class="depth02_gnb depth02_{{$count}} gnb_serise">
            <nav>
                <ul>
                    <li class="first"></li>
                    @foreach($category['children'] as $item)
                        <li>
                            <a href="{{route('product.category',$item['slug'])}}">{{$item['name']}}</a>
                        </li>
                    @endforeach
                </ul>
            </nav>
        </div>
    @endif

    @if($category->children->count() || $category->products->count())
        <?php $count++ ?>
    @endif
@endforeach




@section('end')
    <script type="text/javascript">
        $('.depth03').prev().addClass('on');
        $('.depth03').show();
    </script>
@endsection



