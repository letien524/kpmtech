<footer>
    <div class="content">
        <div class="item1">
            <div class="container">
                <div class="row">
                    @if($footerContacts->count())
                    <div class="col-md-4 wow fadeInUp wHighlight" data-wow-delay=".25s">
                        <div class="item">
                            <div class="title">{{__('Văn phòng của chúng tôi')}}</div>
                            @foreach($footerContacts as $contact)
                                @if(strlen(@$contact['province'][$currentLang]))
                                <div class="info-title"><span>{{$contact['province'][$currentLang]}}</span></div>
                                @endif
                                <div class="info">
                                    @if(strlen(@$contact['address'][$currentLang]))
                                        <p><span>{{__('Địa chỉ')}}: {!! $contact['address'][$currentLang] !!}</span> </p>
                                    @endif

                                    @if(strlen(@$contact['email']))
                                        <p>{{__('Email')}}: <a href="{{$contact['email']}} " style="color:inherit" title="{{$contact['email']}}">{{$contact['email']}} </a></p>
                                    @endif

                                    @if(strlen(@$contact['hotline']))
                                        <p>{{__('Hotline')}}: <a href="{{$contact['hotline']}}" style="color:inherit" title="{{$contact['hotline']}}">{{$contact['hotline']}}</a></p>
                                    @endif

                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                    @if(count((array)@$site_footer['social']))
                    <div class="col-md-4 wow fadeInUp wHighlight" data-wow-delay=".35s">
                        <div class="item">
                            <div class="title">{{__('Kết nối với chúng tôi')}}</div>
                            <div class="info-title"><span>{{__('các kênh mạng xã hội')}}</span></div>
                            <div class="info">
                                <div class="sotical">
                                    <ul class="list-inline">
                                        @foreach($site_footer['social'] as $social)
                                        <li class="list-inline-item">
                                            <a href="{{@$social['link']}}" target="_blank"><i class="fa {{@$social['icon']}}" title="{{@$social['name']}}"></i></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="col-md-4 wow fadeInUp wHighlight" data-wow-delay=".45s">
                        <div class="item">
                            <div class="title">{{__('Đăng ký nhận tin')}}</div>
                            <div class="info-title"><span>{{__('đăng ký nhận ưu đãi đặc biệt')}}</span></div>
                            <div class="info">
                                <form action="{{route('contact.register')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="email" name="email" value="{{old('email')}}" placeholder="Email" class="inp-regis">
                                    <button type="submit" class="btn-sent"><i class="fa fa-arrow-right"></i></button>
                                    {!! @$site_footer['news_register'][$currentLang] !!}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item2">
            @if(strlen(@$site_footer['map']))
                <div class="container">
                    <div class="maps wow fadeInUp wHighlight" data-wow-delay=".25s">
                        {!! $site_footer['map'] !!}
                    </div>
                </div>
            @endif

            <div class="menu-footer">
                <div class="container">
                    <div class="item-menu-footer">
                        <div class="row">
                            <div class="col-md-9 wow fadeInLeft wHighlight" data-wow-delay=".25s">
                                <ul class="list-inline">
                                    @include('components.menu',['class'=>'list-inline-item'])
                                </ul>
                            </div>
                            <div class="col-md-3 wow fadeInRight wHighlight" data-wow-delay=".25s">
                                <div class="logo-foter pull-right">
                                    <a href="{{route('home')}}"><img src="{{strlen(@$ite_settings['logo_footer']) ? imageUrlRender($ite_settings['logo_footer']) :getAsset('images/logo-footer.png')}}" class="img-fluid" alt="{{@$site_settings[getColumnByLang('meta_title')]}}"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="allright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 wow fadeInLeft wHighlight" data-wow-delay=".25s">
                            <div class="left">
                                <span>© GCO GROUP 2017. All rights reserved</span>
                            </div>
                        </div>
                        <div class="col-md-3 wow fadeInRight wHighlight" data-wow-delay=".25s">
                            <div class="right pull-right">
                                <span>Design BY</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>