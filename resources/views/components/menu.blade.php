<li class="{{@$class}}"><a href="{{route('home')}}">{{__('Trang chủ')}}</a></li>
<li class="{{@$class}}"><a href="{{route('about')}}">{{__('Giới thiệu')}}</a></li>
<li class="{{@$class}}"><a href="{{route('room')}}">{{__('Phòng')}}</a></li>
<li class="{{@$class}}"><a href="{{route('service')}}">{{__('Dịch vụ')}}</a></li>
<li class="{{@$class}}"><a href="{{route('blog')}}">{{__('Tin tức')}}</a></li>
<li class="{{@$class}}"><a href="{{route('contact')}}">{{__('Liên hệ')}}</a></li>