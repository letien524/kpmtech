<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailToAdminWithNewCustomer extends Mailable
{
    use Queueable, SerializesModels;

    protected $customer;


    public function __construct(Contact $customer)
    {
        $this->customer = $customer;
    }


    public function build()
    {
        $customer = $this->customer;
        $subject = "{$customer['email']} - Vừa gửi liên hệ mới cho bạn!";

        $data = [
            'customer' => $customer
        ];

        return $this->subject($subject)
            ->view('mail.new-customer',$data);
    }
}
