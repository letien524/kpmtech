<?php

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blogs')->delete();
        
        \DB::table('blogs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'NHẬP KHẨU HÓA CHẤT TỪ TRUNG QUỐC GIA TĂNG MẠNH NĂM 2016',
                'slug' => 'nhap-khau-hoa-chat-tu-trung-quoc-gia-tang-manh-nam-2016',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:07:55',
                'updated_at' => '2019-08-24 21:07:55',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Quản lý tiền chất chặt nhưng không được ảnh hưởng tới hoạt động doanh nghiệp',
                'slug' => 'quan-ly-tien-chat-chat-nhung-khong-duoc-anh-huong-toi-hoat-dong-doanh-nghiep',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:08:30',
                'updated_at' => '2019-08-24 21:08:30',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Ngành công nghiệp hóa chất Việt Nam là ngành mũi nhọn trong các ngành công nghiệp',
                'slug' => 'nganh-cong-nghiep-hoa-chat-viet-nam-la-nganh-mui-nhon-trong-cac-nganh-cong-nghiep',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:08:43',
                'updated_at' => '2019-08-24 21:08:43',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Vai trò của hóa chất trong đời sống',
                'slug' => 'vai-tro-cua-hoa-chat-trong-doi-song',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:08:55',
                'updated_at' => '2019-08-24 21:08:55',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Hóa chất là gì',
                'slug' => 'hoa-chat-la-gi',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).</p>

<p><strong>CôngThương</strong> - <strong><em>* Thưa ông, hiện dư luận rất lo lắng vì tình trạng tiền chất đang trôi nổi khá nhiều trên thị trường và là nguyên nhân gây nên tình trạng gia tăng tội phạm ma túy. Ông có ý kiến gì về vấn đề này?</em></strong></p>

<p>- Hiện tại Chính phủ đã quy định 42 loại tiền chất phải quản lý, kiểm soát chặt chẽ tránh để kẻ xấu sử dụng vào sản xuất ma túy tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản lý 10 loại, Bộ Công Thương quản lý 32 loại (30 loại do Cục Hóa chất cấp giấy phép, 2 loại do Vụ Xuất nhập khẩu cấp phép tạm nhập, tái xuất). Trong số 42 loại tiền chất phải quản lý có 18 loại tiền chất thiết yếu (Bộ Công Thương quản lý 10 loại, Bộ Y tế quản lý 8 loại). Hầu hết các loại tiền chất đều phải nhập khẩu, chỉ có một số ít do các doanh nghiệp trong nước sản xuất được như các axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong các ngành công nghiệp, sản xuất hàng tiêu dùng, mỹ phẩm, y tế và nghiên cứu khoa học. Vì vậy, chúng ta không thể dùng giải pháp “không quản được thì cấm”. Tuy nhiên, nhiều hóa chất thông dụng có thể sử dụng sản xuất ma túy cũng lại là những hóa chất cơ bản không thể thiếu trong ngành công nghiệp hóa chất như axit acetic, axit sunlfuric, toluene … Chính vì vậy, nếu không nắm rõ được mục đích sử dụng của các đối tượng thì khó có thể phân biệt được khi nào nó đóng vai trò là hóa chất công nghiệp, khi nào nó là tiền chất sản xuất ma túy.</p>

<p><strong><em>* Tại sao việc quản lý tiền chất lại khó khăn. Bộ Công Thương có vai trò như thế nào trong lĩnh vực này, thưa ông?</em></strong></p>

<p>- Cùng với tiền chất vũ khí hóa học, tiền chất vật liệu nổ thì tiền chất ma túy được xếp vào loại hóa chất lưỡng dụng. Tính hai mặt của các loại tiền chất đang đặt ra những khó khăn nhất định cho các cơ quan quản lý. Yêu cầu đặt ra đối với công tác quản lý, kiểm soát tiền chất là vừa đảm bảo mục tiêu phát triển kinh tế- xã hội, vừa phòng ngừa thất thoát tiền chất vào các mục đích bất hợp pháp.</p>

<p>Điều 41 Luật Phòng, chống ma túy quy định: Bộ Công Thương có trách nhiệm ban hành danh mục, quy chế quản lý tiền chất sử dụng trong lĩnh vực công nghiệp và tổ chức thực hiện quy chế đó. Bộ trưởng Bộ Công Thương cấp, thu hồi giấy phép nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ Công Thương đã ban hành 4 văn bản quản lý kiểm soát tiền chất. Tuy nhiên, việc quản lý còn có những khó khăn, bất cập nhất định, thí dụ Quy chế quản lý tiền chất của Bộ Công Thương chưa phân định được cấp độ quản lý; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh và sử dụng tiền chất trong lĩnh vực công nghiệp chưa đầy đủ. Ngoài ra việc theo dõi chống thất thoát tiền chất ở các khâu xuất nhập khẩu, lưu thông, sử dụng và tồn trữ tiền chất còn nhiều bất cập. Các đơn vị nhập khẩu tiền chất về để kinh doanh không chỉ bán lại cho các đối tượng trực tiếp sử dụng mà còn bán cho nhiều đối tượng kinh doanh khác; Các đơn vị này lại bán cho các đối tượng khác tạo thành một khâu trung chuyển phức tạp, do đó việc kiểm soát đến khâu cuối cùng còn nhiều khó khăn.</p>

<p><strong><em>* Theo ông cần có giải pháp nào để kiểm soát tiền chất trong công nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, không có tiền chất sẽ không có ma túy. Muốn xã hội trong sạch và lành mạnh thì yếu tố đầu tiên là không được để thất thoát tiền chất vào sản xuất ma túy. Để nâng cao hiệu quả quản lý, kiểm soát tiền chất trong tình hình mới, trước hết cần có sự đánh giá phân loại tiền chất theo các cấp độ khác nhau. Trong đó, tiền chất nguy cơ cao là những hóa chất được sử dụng để điều chế, sản xuất ma túy, nó tham gia một phần hoặc toàn bộ vào cấu trúc phân tử cuối cùng của chất ma túy. Tiền chất thông thường là những hóa chất được sử dụng làm chất phản ứng hoặc làm dung môi, chất thử trong điều chế bất hợp pháp các chất ma túy tại Việt Nam. Từ cơ sở phân loại sẽ hoàn thiện hệ thống pháp luật và cơ chế, chính sách quản lý, kiểm soát tiền chất phù hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản lý tiền chất ở các nước trên thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghiên cứu phương pháp, chủng loại nguyên liệu từ các vụ điều chế trong nước để xác định các tiền chất có nguy cơ cao tại Việt Nam để tìm giải pháp quản lý thích hợp. Mục tiêu cuối cùng là nâng cao hiệu quả quản lý, kiểm soát tiền chất nhưng không làm ảnh hưởng tới các hoạt động sản xuất, kinh doanh của các tổ chức cá nhân có hoạt động liên quan đến tiền chất.</p>

<p><strong><em>* Xin ơn ông!</em></strong></p>

<p align="center"><strong>Quy định mới về quản lý tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ Công Thương đang xây dựng dự thảo Thông tư quy định về quản lý kiểm soát tiền chất công nghiệp trên cơ sở hợp nhất các văn bản quy phạm pháp luật hiện hành. Dự thảo Thông tư sẽ quy định theo hướng: phân loại tiền chất theo cấp độ để quản lý; quản lý chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ các tổ chức, cá nhân tham gia vào việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất…</p>

<p>Cụ thể: Việc phân phối, mua bán, trao đổi tiền chất phải có Hợp đồng mua bán tiền chất hoặc một trong các tài liệu: hợp đồng nguyên tắc; thỏa thuận bán hàng, mua hàng; bản ghi nhớ. Trong hợp đồng phải có nội dung thông tin cảnh báo: Tiền chất có thể bị lợi dụng vào sản xuất ma túy; không được sử dụng tiền chất vào mục đích bất hợp pháp. Trường hợp mua bán tiền chất không có hóa đơn, chứng từ đều bị coi là kinh doanh trái phép và bị xử lý theo quy định của pháp luật.</p>

<p>Tổ chức, cá nhân kinh doanh tiền chất phải lập sổ theo dõi các thông tin: Họ và tên khách hàng; địa chỉ; số điện thoại, số fax; năm thành lập; khách hàng tiếp tục bán lại hay là nơi sử dụng tiền chất cuối cùng; mục đích của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất thì phải nêu tên sản phẩm); số lượng tiền chất bán ra hàng ngày. Sổ theo dõi mua bán tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, cá nhân sử dụng tiền chất phải lập sổ riêng để theo dõi: số lượng tiền chất sử dụng hàng ngày, thời gian và mục đích sử dụng, định mức tiêu hao tiền chất trên một đơn vị sản phẩm. Sổ này cũng phải được lưu giữ trong 5 năm. Trong quá trình sử dụng và tồn trữ tiền chất, nếu phát hiện có thất thoát tiền chất phải báo ngay cho cơ quan công an và cơ quan quản lý chuyên ngành hóa chất tại địa bàn hoạt động để có biện pháp ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy phép nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu vào Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngoài theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ngày 29 tháng 5 năm 2003 của Chính phủ quy định về kiểm soát nhập khẩu, xuất khẩu, vận chuyển quá cảnh lãnh thổ Việt Nam chất ma túy, tiền chất, thuốc gây nghiện, thuốc hướng thần và có thời hạn trong vòng 03 (ba) tháng, kể từ ngày cấp phép. Trường hợp hết thời hạn ghi trong giấy phép nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong thì đề nghị Bộ Công thương (Cục Hóa chất) xem xét gia hạn thêm. Giấy phép chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn không quá 03 (ba) tháng.</p>

<p>Doanh nghiệp chế xuất khi bán tiền chất vào nội địa hoặc doanh nghiệp nội địa khi bán tiền chất cho doanh nghiệp chế xuất phải thực hiện các quy định về xuất nhập khẩu và làm thủ tục hải quan theo quy định hiện hành. Hàng năm, Cục Hóa chất có nhiệm vụ chủ trì phối hợp với Văn phòng Thường trực phòng, chống tội phạm và ma túy thuộc Bộ Công an, Cục Quản lý thị trường và các cơ quan có liên quan kiểm tra định kỳ hoặc đột xuất việc thực hiện các quy định về quản lý, kiểm soát tiền chất. Các Sở Công thương cũng phối hợp với Cục Hóa chất, công an tỉnh, thành phố; Chi cục quản lý thị trường và các cơ quan có liên quan kiểm tra định kỳ hoặc đột xuất các yêu cầu về kinh doanh tiền chất đối với các tổ chức, cá nhân kinh doanh tiền chất trên địa bàn.</p>

<p>Cũng theo Dự thảo, Bộ Công thương có quyền ra quyết định thu hồi giấy phép và đình chỉ việc nhập khẩu, xuất khẩu khi các tổ chức cá nhân vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:09:06',
                'updated_at' => '2019-08-24 21:09:06',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Lưu huỳnh công nghiệp với sức khỏe con người',
                'slug' => 'luu-huynh-cong-nghiep-voi-suc-khoe-con-nguoi',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:09:25',
                'updated_at' => '2019-08-24 21:09:25',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Từ 1/7, Việt Nam sẽ bỏ tù người sản xuất – kinh doanh hóa chất cấm, thay vì chỉ phạt 10 triệu đồng',
                'slug' => 'tu-17-viet-nam-se-bo-tu-nguoi-san-xuat-–-kinh-doanh-hoa-chat-cam-thay-vi-chi-phat-10-trieu-dong',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).</p>

<p><strong>CôngThương</strong> - <strong><em>* Thưa ông, hiện dư luận rất lo lắng vì tình trạng tiền chất đang trôi nổi khá nhiều trên thị trường và là nguyên nhân gây nên tình trạng gia tăng tội phạm ma túy. Ông có ý kiến gì về vấn đề này?</em></strong></p>

<p>- Hiện tại Chính phủ đã quy định 42 loại tiền chất phải quản lý, kiểm soát chặt chẽ tránh để kẻ xấu sử dụng vào sản xuất ma túy tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản lý 10 loại, Bộ Công Thương quản lý 32 loại (30 loại do Cục Hóa chất cấp giấy phép, 2 loại do Vụ Xuất nhập khẩu cấp phép tạm nhập, tái xuất). Trong số 42 loại tiền chất phải quản lý có 18 loại tiền chất thiết yếu (Bộ Công Thương quản lý 10 loại, Bộ Y tế quản lý 8 loại). Hầu hết các loại tiền chất đều phải nhập khẩu, chỉ có một số ít do các doanh nghiệp trong nước sản xuất được như các axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong các ngành công nghiệp, sản xuất hàng tiêu dùng, mỹ phẩm, y tế và nghiên cứu khoa học. Vì vậy, chúng ta không thể dùng giải pháp “không quản được thì cấm”. Tuy nhiên, nhiều hóa chất thông dụng có thể sử dụng sản xuất ma túy cũng lại là những hóa chất cơ bản không thể thiếu trong ngành công nghiệp hóa chất như axit acetic, axit sunlfuric, toluene … Chính vì vậy, nếu không nắm rõ được mục đích sử dụng của các đối tượng thì khó có thể phân biệt được khi nào nó đóng vai trò là hóa chất công nghiệp, khi nào nó là tiền chất sản xuất ma túy.</p>

<p><strong><em>* Tại sao việc quản lý tiền chất lại khó khăn. Bộ Công Thương có vai trò như thế nào trong lĩnh vực này, thưa ông?</em></strong></p>

<p>- Cùng với tiền chất vũ khí hóa học, tiền chất vật liệu nổ thì tiền chất ma túy được xếp vào loại hóa chất lưỡng dụng. Tính hai mặt của các loại tiền chất đang đặt ra những khó khăn nhất định cho các cơ quan quản lý. Yêu cầu đặt ra đối với công tác quản lý, kiểm soát tiền chất là vừa đảm bảo mục tiêu phát triển kinh tế- xã hội, vừa phòng ngừa thất thoát tiền chất vào các mục đích bất hợp pháp.</p>

<p>Điều 41 Luật Phòng, chống ma túy quy định: Bộ Công Thương có trách nhiệm ban hành danh mục, quy chế quản lý tiền chất sử dụng trong lĩnh vực công nghiệp và tổ chức thực hiện quy chế đó. Bộ trưởng Bộ Công Thương cấp, thu hồi giấy phép nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ Công Thương đã ban hành 4 văn bản quản lý kiểm soát tiền chất. Tuy nhiên, việc quản lý còn có những khó khăn, bất cập nhất định, thí dụ Quy chế quản lý tiền chất của Bộ Công Thương chưa phân định được cấp độ quản lý; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh và sử dụng tiền chất trong lĩnh vực công nghiệp chưa đầy đủ. Ngoài ra việc theo dõi chống thất thoát tiền chất ở các khâu xuất nhập khẩu, lưu thông, sử dụng và tồn trữ tiền chất còn nhiều bất cập. Các đơn vị nhập khẩu tiền chất về để kinh doanh không chỉ bán lại cho các đối tượng trực tiếp sử dụng mà còn bán cho nhiều đối tượng kinh doanh khác; Các đơn vị này lại bán cho các đối tượng khác tạo thành một khâu trung chuyển phức tạp, do đó việc kiểm soát đến khâu cuối cùng còn nhiều khó khăn.</p>

<p><strong><em>* Theo ông cần có giải pháp nào để kiểm soát tiền chất trong công nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, không có tiền chất sẽ không có ma túy. Muốn xã hội trong sạch và lành mạnh thì yếu tố đầu tiên là không được để thất thoát tiền chất vào sản xuất ma túy. Để nâng cao hiệu quả quản lý, kiểm soát tiền chất trong tình hình mới, trước hết cần có sự đánh giá phân loại tiền chất theo các cấp độ khác nhau. Trong đó, tiền chất nguy cơ cao là những hóa chất được sử dụng để điều chế, sản xuất ma túy, nó tham gia một phần hoặc toàn bộ vào cấu trúc phân tử cuối cùng của chất ma túy. Tiền chất thông thường là những hóa chất được sử dụng làm chất phản ứng hoặc làm dung môi, chất thử trong điều chế bất hợp pháp các chất ma túy tại Việt Nam. Từ cơ sở phân loại sẽ hoàn thiện hệ thống pháp luật và cơ chế, chính sách quản lý, kiểm soát tiền chất phù hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản lý tiền chất ở các nước trên thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghiên cứu phương pháp, chủng loại nguyên liệu từ các vụ điều chế trong nước để xác định các tiền chất có nguy cơ cao tại Việt Nam để tìm giải pháp quản lý thích hợp. Mục tiêu cuối cùng là nâng cao hiệu quả quản lý, kiểm soát tiền chất nhưng không làm ảnh hưởng tới các hoạt động sản xuất, kinh doanh của các tổ chức cá nhân có hoạt động liên quan đến tiền chất.</p>

<p><strong><em>* Xin ơn ông!</em></strong></p>

<p align="center"><strong>Quy định mới về quản lý tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ Công Thương đang xây dựng dự thảo Thông tư quy định về quản lý kiểm soát tiền chất công nghiệp trên cơ sở hợp nhất các văn bản quy phạm pháp luật hiện hành. Dự thảo Thông tư sẽ quy định theo hướng: phân loại tiền chất theo cấp độ để quản lý; quản lý chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ các tổ chức, cá nhân tham gia vào việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất…</p>

<p>Cụ thể: Việc phân phối, mua bán, trao đổi tiền chất phải có Hợp đồng mua bán tiền chất hoặc một trong các tài liệu: hợp đồng nguyên tắc; thỏa thuận bán hàng, mua hàng; bản ghi nhớ. Trong hợp đồng phải có nội dung thông tin cảnh báo: Tiền chất có thể bị lợi dụng vào sản xuất ma túy; không được sử dụng tiền chất vào mục đích bất hợp pháp. Trường hợp mua bán tiền chất không có hóa đơn, chứng từ đều bị coi là kinh doanh trái phép và bị xử lý theo quy định của pháp luật.</p>

<p>Tổ chức, cá nhân kinh doanh tiền chất phải lập sổ theo dõi các thông tin: Họ và tên khách hàng; địa chỉ; số điện thoại, số fax; năm thành lập; khách hàng tiếp tục bán lại hay là nơi sử dụng tiền chất cuối cùng; mục đích của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất thì phải nêu tên sản phẩm); số lượng tiền chất bán ra hàng ngày. Sổ theo dõi mua bán tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, cá nhân sử dụng tiền chất phải lập sổ riêng để theo dõi: số lượng tiền chất sử dụng hàng ngày, thời gian và mục đích sử dụng, định mức tiêu hao tiền chất trên một đơn vị sản phẩm. Sổ này cũng phải được lưu giữ trong 5 năm. Trong quá trình sử dụng và tồn trữ tiền chất, nếu phát hiện có thất thoát tiền chất phải báo ngay cho cơ quan công an và cơ quan quản lý chuyên ngành hóa chất tại địa bàn hoạt động để có biện pháp ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy phép nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu vào Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngoài theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ngày 29 tháng 5 năm 2003 của Chính phủ quy định về kiểm soát nhập khẩu, xuất khẩu, vận chuyển quá cảnh lãnh thổ Việt Nam chất ma túy, tiền chất, thuốc gây nghiện, thuốc hướng thần và có thời hạn trong vòng 03 (ba) tháng, kể từ ngày cấp phép. Trường hợp hết thời hạn ghi trong giấy phép nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong thì đề nghị Bộ Công thương (Cục Hóa chất) xem xét gia hạn thêm. Giấy phép chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn không quá 03 (ba) tháng.</p>

<p>Doanh nghiệp chế xuất khi bán tiền chất vào nội địa hoặc doanh nghiệp nội địa khi bán tiền chất cho doanh nghiệp chế xuất phải thực hiện các quy định về xuất nhập khẩu và làm thủ tục hải quan theo quy định hiện hành. Hàng năm, Cục Hóa chất có nhiệm vụ chủ trì phối hợp với Văn phòng Thường trực phòng, chống tội phạm và ma túy thuộc Bộ Công an, Cục Quản lý thị trường và các cơ quan có liên quan kiểm tra định kỳ hoặc đột xuất việc thực hiện các quy định về quản lý, kiểm soát tiền chất. Các Sở Công thương cũng phối hợp với Cục Hóa chất, công an tỉnh, thành phố; Chi cục quản lý thị trường và các cơ quan có liên quan kiểm tra định kỳ hoặc đột xuất các yêu cầu về kinh doanh tiền chất đối với các tổ chức, cá nhân kinh doanh tiền chất trên địa bàn.</p>

<p>Cũng theo Dự thảo, Bộ Công thương có quyền ra quyết định thu hồi giấy phép và đình chỉ việc nhập khẩu, xuất khẩu khi các tổ chức cá nhân vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:09:37',
                'updated_at' => '2019-08-24 21:09:37',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Thủ tục Cấp giấy phép vận chuyển hóa chất nguy hiểm',
                'slug' => 'thu-tuc-cap-giay-phep-van-chuyen-hoa-chat-nguy-hiem',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).</p>

<p><strong>CôngThương</strong> - <strong><em>* Thưa ông, hiện dư luận rất lo lắng vì tình trạng tiền chất đang trôi nổi khá nhiều trên thị trường và là nguyên nhân gây nên tình trạng gia tăng tội phạm ma túy. Ông có ý kiến gì về vấn đề này?</em></strong></p>

<p>- Hiện tại Chính phủ đã quy định 42 loại tiền chất phải quản lý, kiểm soát chặt chẽ tránh để kẻ xấu sử dụng vào sản xuất ma túy tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản lý 10 loại, Bộ Công Thương quản lý 32 loại (30 loại do Cục Hóa chất cấp giấy phép, 2 loại do Vụ Xuất nhập khẩu cấp phép tạm nhập, tái xuất). Trong số 42 loại tiền chất phải quản lý có 18 loại tiền chất thiết yếu (Bộ Công Thương quản lý 10 loại, Bộ Y tế quản lý 8 loại). Hầu hết các loại tiền chất đều phải nhập khẩu, chỉ có một số ít do các doanh nghiệp trong nước sản xuất được như các axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong các ngành công nghiệp, sản xuất hàng tiêu dùng, mỹ phẩm, y tế và nghiên cứu khoa học. Vì vậy, chúng ta không thể dùng giải pháp “không quản được thì cấm”. Tuy nhiên, nhiều hóa chất thông dụng có thể sử dụng sản xuất ma túy cũng lại là những hóa chất cơ bản không thể thiếu trong ngành công nghiệp hóa chất như axit acetic, axit sunlfuric, toluene … Chính vì vậy, nếu không nắm rõ được mục đích sử dụng của các đối tượng thì khó có thể phân biệt được khi nào nó đóng vai trò là hóa chất công nghiệp, khi nào nó là tiền chất sản xuất ma túy.</p>

<p><strong><em>* Tại sao việc quản lý tiền chất lại khó khăn. Bộ Công Thương có vai trò như thế nào trong lĩnh vực này, thưa ông?</em></strong></p>

<p>- Cùng với tiền chất vũ khí hóa học, tiền chất vật liệu nổ thì tiền chất ma túy được xếp vào loại hóa chất lưỡng dụng. Tính hai mặt của các loại tiền chất đang đặt ra những khó khăn nhất định cho các cơ quan quản lý. Yêu cầu đặt ra đối với công tác quản lý, kiểm soát tiền chất là vừa đảm bảo mục tiêu phát triển kinh tế- xã hội, vừa phòng ngừa thất thoát tiền chất vào các mục đích bất hợp pháp.</p>

<p>Điều 41 Luật Phòng, chống ma túy quy định: Bộ Công Thương có trách nhiệm ban hành danh mục, quy chế quản lý tiền chất sử dụng trong lĩnh vực công nghiệp và tổ chức thực hiện quy chế đó. Bộ trưởng Bộ Công Thương cấp, thu hồi giấy phép nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ Công Thương đã ban hành 4 văn bản quản lý kiểm soát tiền chất. Tuy nhiên, việc quản lý còn có những khó khăn, bất cập nhất định, thí dụ Quy chế quản lý tiền chất của Bộ Công Thương chưa phân định được cấp độ quản lý; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh và sử dụng tiền chất trong lĩnh vực công nghiệp chưa đầy đủ. Ngoài ra việc theo dõi chống thất thoát tiền chất ở các khâu xuất nhập khẩu, lưu thông, sử dụng và tồn trữ tiền chất còn nhiều bất cập. Các đơn vị nhập khẩu tiền chất về để kinh doanh không chỉ bán lại cho các đối tượng trực tiếp sử dụng mà còn bán cho nhiều đối tượng kinh doanh khác; Các đơn vị này lại bán cho các đối tượng khác tạo thành một khâu trung chuyển phức tạp, do đó việc kiểm soát đến khâu cuối cùng còn nhiều khó khăn.</p>

<p><strong><em>* Theo ông cần có giải pháp nào để kiểm soát tiền chất trong công nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, không có tiền chất sẽ không có ma túy. Muốn xã hội trong sạch và lành mạnh thì yếu tố đầu tiên là không được để thất thoát tiền chất vào sản xuất ma túy. Để nâng cao hiệu quả quản lý, kiểm soát tiền chất trong tình hình mới, trước hết cần có sự đánh giá phân loại tiền chất theo các cấp độ khác nhau. Trong đó, tiền chất nguy cơ cao là những hóa chất được sử dụng để điều chế, sản xuất ma túy, nó tham gia một phần hoặc toàn bộ vào cấu trúc phân tử cuối cùng của chất ma túy. Tiền chất thông thường là những hóa chất được sử dụng làm chất phản ứng hoặc làm dung môi, chất thử trong điều chế bất hợp pháp các chất ma túy tại Việt Nam. Từ cơ sở phân loại sẽ hoàn thiện hệ thống pháp luật và cơ chế, chính sách quản lý, kiểm soát tiền chất phù hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản lý tiền chất ở các nước trên thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghiên cứu phương pháp, chủng loại nguyên liệu từ các vụ điều chế trong nước để xác định các tiền chất có nguy cơ cao tại Việt Nam để tìm giải pháp quản lý thích hợp. Mục tiêu cuối cùng là nâng cao hiệu quả quản lý, kiểm soát tiền chất nhưng không làm ảnh hưởng tới các hoạt động sản xuất, kinh doanh của các tổ chức cá nhân có hoạt động liên quan đến tiền chất.</p>

<p><strong><em>* Xin ơn ông!</em></strong></p>

<p align="center"><strong>Quy định mới về quản lý tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ Công Thương đang xây dựng dự thảo Thông tư quy định về quản lý kiểm soát tiền chất công nghiệp trên cơ sở hợp nhất các văn bản quy phạm pháp luật hiện hành. Dự thảo Thông tư sẽ quy định theo hướng: phân loại tiền chất theo cấp độ để quản lý; quản lý chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ các tổ chức, cá nhân tham gia vào việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất…</p>

<p>Cụ thể: Việc phân phối, mua bán, trao đổi tiền chất phải có Hợp đồng mua bán tiền chất hoặc một trong các tài liệu: hợp đồng nguyên tắc; thỏa thuận bán hàng, mua hàng; bản ghi nhớ. Trong hợp đồng phải có nội dung thông tin cảnh báo: Tiền chất có thể bị lợi dụng vào sản xuất ma túy; không được sử dụng tiền chất vào mục đích bất hợp pháp. Trường hợp mua bán tiền chất không có hóa đơn, chứng từ đều bị coi là kinh doanh trái phép và bị xử lý theo quy định của pháp luật.</p>

<p>Tổ chức, cá nhân kinh doanh tiền chất phải lập sổ theo dõi các thông tin: Họ và tên khách hàng; địa chỉ; số điện thoại, số fax; năm thành lập; khách hàng tiếp tục bán lại hay là nơi sử dụng tiền chất cuối cùng; mục đích của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất thì phải nêu tên sản phẩm); số lượng tiền chất bán ra hàng ngày. Sổ theo dõi mua bán tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, cá nhân sử dụng tiền chất phải lập sổ riêng để theo dõi: số lượng tiền chất sử dụng hàng ngày, thời gian và mục đích sử dụng, định mức tiêu hao tiền chất trên một đơn vị sản phẩm. Sổ này cũng phải được lưu giữ trong 5 năm. Trong quá trình sử dụng và tồn trữ tiền chất, nếu phát hiện có thất thoát tiền chất phải báo ngay cho cơ quan công an và cơ quan quản lý chuyên ngành hóa chất tại địa bàn hoạt động để có biện pháp ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy phép nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu vào Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngoài theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ngày 29 tháng 5 năm 2003 của Chính phủ quy định về kiểm soát nhập khẩu, xuất khẩu, vận chuyển quá cảnh lãnh thổ Việt Nam chất ma túy, tiền chất, thuốc gây nghiện, thuốc hướng thần và có thời hạn trong vòng 03 (ba) tháng, kể từ ngày cấp phép. Trường hợp hết thời hạn ghi trong giấy phép nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong thì đề nghị Bộ Công thương (Cục Hóa chất) xem xét gia hạn thêm. Giấy phép chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn không quá 03 (ba) tháng.</p>

<p>Doanh nghiệp chế xuất khi bán tiền chất vào nội địa hoặc doanh nghiệp nội địa khi bán tiền chất cho doanh nghiệp chế xuất phải thực hiện các quy định về xuất nhập khẩu và làm thủ tục hải quan theo quy định hiện hành. Hàng năm, Cục Hóa chất có nhiệm vụ chủ trì phối hợp với Văn phòng Thường trực phòng, chống tội phạm và ma túy thuộc Bộ Công an, Cục Quản lý thị trường và các cơ quan có liên quan kiểm tra định kỳ hoặc đột xuất việc thực hiện các quy định về quản lý, kiểm soát tiền chất. Các Sở Công thương cũng phối hợp với Cục Hóa chất, công an tỉnh, thành phố; Chi cục quản lý thị trường và các cơ quan có liên quan kiểm tra định kỳ hoặc đột xuất các yêu cầu về kinh doanh tiền chất đối với các tổ chức, cá nhân kinh doanh tiền chất trên địa bàn.</p>

<p>Cũng theo Dự thảo, Bộ Công thương có quyền ra quyết định thu hồi giấy phép và đình chỉ việc nhập khẩu, xuất khẩu khi các tổ chức cá nhân vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:09:49',
                'updated_at' => '2019-08-24 21:09:49',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Nghiên cứu sản xuất metanol trực tiếp từ khí carbonic',
                'slug' => 'nghien-cuu-san-xuat-metanol-truc-tiep-tu-khi-carbonic',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:10:14',
                'updated_at' => '2019-08-24 21:10:14',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Sản xuất phân bón từ nước thải',
                'slug' => 'san-xuat-phan-bon-tu-nuoc-thai',
            'excerpt' => 'Kiểm soát tiền chất để hạn chế tội phạm ma túy là rất cần thiết nhưng quan trọng là không được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đó là ý kiến của TS. Phùng Hà- Cục trưởng Cục Hóa chất (Bộ Công Thương).',
            'content' => '<p itemprop="description">Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<table>
<tbody>
<tr>
<td>
<p>Kiểm so&aacute;t tiền chất để hạn chế tội phạm ma t&uacute;y l&agrave; rất cần thiết nhưng quan trọng l&agrave; kh&ocirc;ng được để ảnh hưởng tới sản xuất kinh doanh của doanh nghiệp. Đ&oacute; l&agrave; &yacute; kiến của TS. Ph&ugrave;ng H&agrave;- Cục trưởng Cục H&oacute;a chất (Bộ C&ocirc;ng Thương).</p>

<p><strong>C&ocirc;ngThương</strong>&nbsp;-&nbsp;<strong><em>* Thưa &ocirc;ng, hiện dư luận rất lo lắng v&igrave; t&igrave;nh trạng tiền chất đang tr&ocirc;i nổi kh&aacute; nhiều tr&ecirc;n thị trường v&agrave; l&agrave; nguy&ecirc;n nh&acirc;n g&acirc;y n&ecirc;n t&igrave;nh trạng gia tăng tội phạm ma t&uacute;y. &Ocirc;ng c&oacute; &yacute; kiến g&igrave; về vấn đề n&agrave;y?</em></strong></p>

<p>- Hiện tại Ch&iacute;nh phủ đ&atilde; quy định 42 loại tiền chất phải quản l&yacute;, kiểm so&aacute;t chặt chẽ tr&aacute;nh để kẻ xấu sử dụng v&agrave;o sản xuất ma t&uacute;y tổng hợp. Trong số 42 tiền chất, Bộ Y tế quản l&yacute; 10 loại, Bộ C&ocirc;ng Thương quản l&yacute; 32 loại (30 loại do Cục H&oacute;a chất cấp giấy ph&eacute;p, 2 loại do Vụ Xuất nhập khẩu cấp ph&eacute;p tạm nhập, t&aacute;i xuất). Trong số 42 loại tiền chất phải quản l&yacute; c&oacute; 18 loại tiền chất thiết yếu (Bộ C&ocirc;ng Thương quản l&yacute; 10 loại, Bộ Y tế quản l&yacute; 8 loại). Hầu hết c&aacute;c loại tiền chất đều phải nhập khẩu, chỉ c&oacute; một số &iacute;t do c&aacute;c doanh nghiệp trong nước sản xuất được như c&aacute;c axit chlohydric, axit sunphuric. Tiền chất rất quan trọng trong c&aacute;c ng&agrave;nh c&ocirc;ng nghiệp, sản xuất h&agrave;ng ti&ecirc;u d&ugrave;ng, mỹ phẩm, y tế v&agrave; nghi&ecirc;n cứu khoa học. V&igrave; vậy, ch&uacute;ng ta kh&ocirc;ng thể d&ugrave;ng giải ph&aacute;p &ldquo;kh&ocirc;ng quản được th&igrave; cấm&rdquo;. Tuy nhi&ecirc;n, nhiều h&oacute;a chất th&ocirc;ng dụng c&oacute; thể sử dụng sản xuất ma t&uacute;y cũng lại l&agrave; những h&oacute;a chất cơ bản kh&ocirc;ng thể thiếu trong ng&agrave;nh c&ocirc;ng nghiệp h&oacute;a chất như axit acetic, axit sunlfuric, toluene &hellip; Ch&iacute;nh v&igrave; vậy, nếu kh&ocirc;ng nắm r&otilde; được mục đ&iacute;ch sử dụng của c&aacute;c đối tượng th&igrave; kh&oacute; c&oacute; thể ph&acirc;n biệt được khi n&agrave;o n&oacute; đ&oacute;ng vai tr&ograve; l&agrave; h&oacute;a chất c&ocirc;ng nghiệp, khi n&agrave;o n&oacute; l&agrave; tiền chất sản xuất ma t&uacute;y.</p>

<p><strong><em>* Tại sao việc quản l&yacute; tiền chất lại kh&oacute; khăn. Bộ C&ocirc;ng Thương c&oacute; vai tr&ograve; như thế n&agrave;o trong lĩnh vực n&agrave;y, thưa &ocirc;ng?</em></strong></p>

<p>- C&ugrave;ng với tiền chất vũ kh&iacute; h&oacute;a học, tiền chất vật liệu nổ th&igrave; tiền chất ma t&uacute;y được xếp v&agrave;o loại h&oacute;a chất lưỡng dụng. T&iacute;nh hai mặt của c&aacute;c loại tiền chất đang đặt ra những kh&oacute; khăn nhất định cho c&aacute;c cơ quan quản l&yacute;. Y&ecirc;u cầu đặt ra đối với c&ocirc;ng t&aacute;c quản l&yacute;, kiểm so&aacute;t tiền chất l&agrave; vừa đảm bảo mục ti&ecirc;u ph&aacute;t triển kinh tế- x&atilde; hội, vừa ph&ograve;ng ngừa thất tho&aacute;t tiền chất v&agrave;o c&aacute;c mục đ&iacute;ch bất hợp ph&aacute;p.</p>

<p>Điều 41 Luật Ph&ograve;ng, chống ma t&uacute;y quy định: Bộ C&ocirc;ng Thương c&oacute; tr&aacute;ch nhiệm ban h&agrave;nh danh mục, quy chế quản l&yacute; tiền chất sử dụng trong lĩnh vực c&ocirc;ng nghiệp v&agrave; tổ chức thực hiện quy chế đ&oacute;. Bộ trưởng Bộ C&ocirc;ng Thương cấp, thu hồi giấy ph&eacute;p nhập khẩu, xuất khẩu tiền chất sử dụng trong lĩnh vực sản xuất. Cho đến nay, Bộ C&ocirc;ng Thương đ&atilde; ban h&agrave;nh 4 văn bản quản l&yacute; kiểm so&aacute;t tiền chất. Tuy nhi&ecirc;n, việc quản l&yacute; c&ograve;n c&oacute; những kh&oacute; khăn, bất cập nhất định, th&iacute; dụ Quy chế quản l&yacute; tiền chất của Bộ C&ocirc;ng Thương chưa ph&acirc;n định được cấp độ quản l&yacute;; Nhận thức của một số doanh nghiệp tham gia hoạt động xuất nhập khẩu, kinh doanh v&agrave; sử dụng tiền chất trong lĩnh vực c&ocirc;ng nghiệp chưa đầy đủ. Ngo&agrave;i ra việc theo d&otilde;i chống thất tho&aacute;t tiền chất ở c&aacute;c kh&acirc;u xuất nhập khẩu, lưu th&ocirc;ng, sử dụng v&agrave; tồn trữ tiền chất c&ograve;n nhiều bất cập. C&aacute;c đơn vị nhập khẩu tiền chất về để kinh doanh kh&ocirc;ng chỉ b&aacute;n lại cho c&aacute;c đối tượng trực tiếp sử dụng m&agrave; c&ograve;n b&aacute;n cho nhiều đối tượng kinh doanh kh&aacute;c; C&aacute;c đơn vị n&agrave;y lại b&aacute;n cho c&aacute;c đối tượng kh&aacute;c tạo th&agrave;nh một kh&acirc;u trung chuyển phức tạp, do đ&oacute; việc kiểm so&aacute;t đến kh&acirc;u cuối c&ugrave;ng c&ograve;n nhiều kh&oacute; khăn.</p>

<p><strong><em>* Theo &ocirc;ng cần c&oacute; giải ph&aacute;p n&agrave;o để kiểm so&aacute;t tiền chất trong c&ocirc;ng nghiệp?</em></strong></p>

<p>- Cần phải khẳng định, kh&ocirc;ng c&oacute; tiền chất sẽ kh&ocirc;ng c&oacute; ma t&uacute;y. Muốn x&atilde; hội trong sạch v&agrave; l&agrave;nh mạnh th&igrave; yếu tố đầu ti&ecirc;n l&agrave; kh&ocirc;ng được để thất tho&aacute;t tiền chất v&agrave;o sản xuất ma t&uacute;y. Để n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất trong t&igrave;nh h&igrave;nh mới, trước hết cần c&oacute; sự đ&aacute;nh gi&aacute; ph&acirc;n loại tiền chất theo c&aacute;c cấp độ kh&aacute;c nhau. Trong đ&oacute;, tiền chất nguy cơ cao l&agrave; những h&oacute;a chất được sử dụng để điều chế, sản xuất ma t&uacute;y, n&oacute; tham gia một phần hoặc to&agrave;n bộ v&agrave;o cấu tr&uacute;c ph&acirc;n tử cuối c&ugrave;ng của chất ma t&uacute;y. Tiền chất th&ocirc;ng thường l&agrave; những h&oacute;a chất được sử dụng l&agrave;m chất phản ứng hoặc l&agrave;m dung m&ocirc;i, chất thử trong điều chế bất hợp ph&aacute;p c&aacute;c chất ma t&uacute;y tại Việt Nam. Từ cơ sở ph&acirc;n loại sẽ ho&agrave;n thiện hệ thống ph&aacute;p luật v&agrave; cơ chế, ch&iacute;nh s&aacute;ch quản l&yacute;, kiểm so&aacute;t tiền chất ph&ugrave; hợp với điều kiện thực tế ở Việt Nam. Đặc biệt, cần phối hợp linh hoạt giữa việc học tập kinh nghiệm quản l&yacute; tiền chất ở c&aacute;c nước tr&ecirc;n thế giới với việc vận dụng kinh nghiệm thực tiễn tại Việt Nam. Cụ thể, phải nghi&ecirc;n cứu phương ph&aacute;p, chủng loại nguy&ecirc;n liệu từ c&aacute;c vụ điều chế trong nước để x&aacute;c định c&aacute;c tiền chất c&oacute; nguy cơ cao tại Việt Nam để t&igrave;m giải ph&aacute;p quản l&yacute; th&iacute;ch hợp. Mục ti&ecirc;u cuối c&ugrave;ng l&agrave; n&acirc;ng cao hiệu quả quản l&yacute;, kiểm so&aacute;t tiền chất nhưng kh&ocirc;ng l&agrave;m ảnh hưởng tới c&aacute;c hoạt động sản xuất, kinh doanh của c&aacute;c tổ chức c&aacute; nh&acirc;n c&oacute; hoạt động li&ecirc;n quan đến tiền chất.</p>

<p><strong><em>* Xin ơn &ocirc;ng!</em></strong></p>

<p align="center"><strong>Quy định mới về quản l&yacute; tiền chất</strong></p>

<table align="center">
<tbody>
<tr>
<td>
<p>Hiện Bộ C&ocirc;ng Thương đang x&acirc;y dựng dự thảo Th&ocirc;ng tư quy định về quản l&yacute; kiểm so&aacute;t tiền chất c&ocirc;ng nghiệp tr&ecirc;n cơ sở hợp nhất c&aacute;c văn bản quy phạm ph&aacute;p luật hiện h&agrave;nh. Dự thảo Th&ocirc;ng tư sẽ quy định theo hướng: ph&acirc;n loại tiền chất theo cấp độ để quản l&yacute;; quản l&yacute; chặt chẽ kinh doanh tiền chất trong nội địa; tăng cường kiểm tra định kỳ c&aacute;c tổ chức, c&aacute; nh&acirc;n tham gia v&agrave;o việc sản xuất, kinh doanh, nhập khẩu, xuất khẩu, sử dụng, tồn trữ, vận chuyển tiền chất&hellip;</p>

<p>Cụ thể: Việc ph&acirc;n phối, mua b&aacute;n, trao đổi tiền chất phải c&oacute; Hợp đồng mua b&aacute;n tiền chất hoặc một trong c&aacute;c t&agrave;i liệu: hợp đồng nguy&ecirc;n tắc; thỏa thuận b&aacute;n h&agrave;ng, mua h&agrave;ng; bản ghi nhớ. Trong hợp đồng phải c&oacute; nội dung th&ocirc;ng tin cảnh b&aacute;o: Tiền chất c&oacute; thể bị lợi dụng v&agrave;o sản xuất ma t&uacute;y; kh&ocirc;ng được sử dụng tiền chất v&agrave;o mục đ&iacute;ch bất hợp ph&aacute;p. Trường hợp mua b&aacute;n tiền chất kh&ocirc;ng c&oacute; h&oacute;a đơn, chứng từ đều bị coi l&agrave; kinh doanh tr&aacute;i ph&eacute;p v&agrave; bị xử l&yacute; theo quy định của ph&aacute;p luật.</p>

<p>Tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất phải lập sổ theo d&otilde;i c&aacute;c th&ocirc;ng tin: Họ v&agrave; t&ecirc;n kh&aacute;ch h&agrave;ng; địa chỉ; số điện thoại, số fax; năm th&agrave;nh lập; kh&aacute;ch h&agrave;ng tiếp tục b&aacute;n lại hay l&agrave; nơi sử dụng tiền chất cuối c&ugrave;ng; mục đ&iacute;ch của việc sử dụng tiền chất (nếu tiền chất được sử dụng để sản xuất th&igrave; phải n&ecirc;u t&ecirc;n sản phẩm); số lượng tiền chất b&aacute;n ra h&agrave;ng ng&agrave;y. Sổ theo d&otilde;i mua b&aacute;n tiền chất được lưu giữ trong thời hạn 5 năm.</p>

<p>Những đơn vị, c&aacute; nh&acirc;n sử dụng tiền chất phải lập sổ ri&ecirc;ng để theo d&otilde;i: số lượng tiền chất sử dụng h&agrave;ng ng&agrave;y, thời gian v&agrave; mục đ&iacute;ch sử dụng, định mức ti&ecirc;u hao tiền chất tr&ecirc;n một đơn vị sản phẩm. Sổ n&agrave;y cũng phải được lưu giữ trong 5 năm. Trong qu&aacute; tr&igrave;nh sử dụng v&agrave; tồn trữ tiền chất, nếu ph&aacute;t hiện c&oacute; thất tho&aacute;t tiền chất phải b&aacute;o ngay cho cơ quan c&ocirc;ng an v&agrave; cơ quan quản l&yacute; chuy&ecirc;n ng&agrave;nh h&oacute;a chất tại địa b&agrave;n hoạt động để c&oacute; biện ph&aacute;p ngăn ngừa, khắc phục hậu quả.</p>

<p>Giấy ph&eacute;p nhập khẩu hoặc xuất khẩu tiền chất được cấp cho từng lần nhập khẩu v&agrave;o Việt Nam hoặc xuất khẩu từ Việt Nam ra nước ngo&agrave;i theo quy định tại Điều 7 Nghị định số 58/2003/NĐ-CP ng&agrave;y 29 th&aacute;ng 5 năm 2003 của Ch&iacute;nh phủ quy định về kiểm so&aacute;t nhập khẩu, xuất khẩu, vận chuyển qu&aacute; cảnh l&atilde;nh thổ Việt Nam chất ma t&uacute;y, tiền chất, thuốc g&acirc;y nghiện, thuốc hướng thần v&agrave; c&oacute; thời hạn trong v&ograve;ng 03 (ba) th&aacute;ng, kể từ ng&agrave;y cấp ph&eacute;p. Trường hợp hết thời hạn ghi trong giấy ph&eacute;p nhưng việc nhập khẩu hoặc xuất khẩu chưa thực hiện được hoặc thực hiện chưa xong th&igrave; đề nghị Bộ C&ocirc;ng thương (Cục H&oacute;a chất) xem x&eacute;t gia hạn th&ecirc;m. Giấy ph&eacute;p chỉ gia hạn một lần trong năm kế hoạch, thời gian gia hạn kh&ocirc;ng qu&aacute; 03 (ba) th&aacute;ng.</p>

<p>Doanh nghiệp chế xuất khi b&aacute;n tiền chất v&agrave;o nội địa hoặc doanh nghiệp nội địa khi b&aacute;n tiền chất cho doanh nghiệp chế xuất phải thực hiện c&aacute;c quy định về xuất nhập khẩu v&agrave; l&agrave;m thủ tục hải quan theo quy định hiện h&agrave;nh. H&agrave;ng năm, Cục H&oacute;a chất c&oacute; nhiệm vụ chủ tr&igrave; phối hợp với Văn ph&ograve;ng Thường trực ph&ograve;ng, chống tội phạm v&agrave; ma t&uacute;y thuộc Bộ C&ocirc;ng an, Cục Quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất việc thực hiện c&aacute;c quy định về quản l&yacute;, kiểm so&aacute;t tiền chất. C&aacute;c Sở C&ocirc;ng thương cũng phối hợp với Cục H&oacute;a chất, c&ocirc;ng an tỉnh, th&agrave;nh phố; Chi cục quản l&yacute; thị trường v&agrave; c&aacute;c cơ quan c&oacute; li&ecirc;n quan kiểm tra định kỳ hoặc đột xuất c&aacute;c y&ecirc;u cầu về kinh doanh tiền chất đối với c&aacute;c tổ chức, c&aacute; nh&acirc;n kinh doanh tiền chất tr&ecirc;n địa b&agrave;n.</p>

<p>Cũng theo Dự thảo, Bộ C&ocirc;ng thương c&oacute; quyền ra quyết định thu hồi giấy ph&eacute;p v&agrave; đ&igrave;nh chỉ việc nhập khẩu, xuất khẩu khi c&aacute;c tổ chức c&aacute; nh&acirc;n vi phạm quy định về xuất nhập khẩu.</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>',
                'image' => '/uploads/images/sp1.jpg',
                'banner' => NULL,
                'display' => 1,
                'meta_image' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-08-24 21:23:05',
                'updated_at' => '2019-08-24 21:23:05',
            ),
        ));
        
        
    }
}