<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Lê Văn Lượng',
                'email' => 'test@gmail.com',
                'phone' => '0978823369',
                'address' => 'Số 6 ngõ 193, Trung Văn, Nam Từ Liên, Hà Nội',
                'content' => '12312314',
                'status' => NULL,
                'created_at' => '2019-08-24 22:36:38',
                'updated_at' => '2019-08-24 22:36:38',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Lê Văn Lượng',
                'email' => 'test@gmail.com',
                'phone' => '0978823369',
                'address' => 'Số 6 ngõ 193, Trung Văn, Nam Từ Liên, Hà Nội',
                'content' => '12312314',
                'status' => 1,
                'created_at' => '2019-08-24 22:37:33',
                'updated_at' => '2019-08-24 22:53:29',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Lê Văn Lượng',
                'email' => 'test@gmail.com',
                'phone' => '0978823369',
                'address' => 'Số 6 ngõ 193, Trung Văn, Nam Từ Liên, Hà Nội',
                'content' => '12312314',
                'status' => NULL,
                'created_at' => '2019-08-24 22:54:03',
                'updated_at' => '2019-08-24 22:54:03',
            ),
        ));
        
        
    }
}