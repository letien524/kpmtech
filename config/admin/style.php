<?php
    return [
        'font-default' => 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        'font-awesome' => 'assets/admin/fonts/font-awesome/css/font-awesome.min.css',
        'bootstrap' => 'assets/admin/plugins/bootstrap/css/bootstrap.min.css',
        'datatable' => 'assets/admin/plugins/datatables/dataTables.bootstrap.css',
        'toastr' => 'assets/admin/plugins/toastr/toastr.min.css',
        'select2' => 'assets/admin/plugins/select2/select2.min.css',
        'main-style' => 'assets/admin/css/AdminLTE.min.css',
        'main-skin' => 'assets/admin/css/skins/_all-skins.min.css',
//        'nestable' => 'assets/admin/plugins/nestable/nestable.css',
        'fontawesome-iconpicker' => 'assets/admin/plugins/fontawesome-iconpicker/css/fontawesome-iconpicker.min.css',
        'customize' => 'assets/admin/customize/style.css',
    ];