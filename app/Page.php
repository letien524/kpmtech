<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Page extends Model
    {
        use ModelTrait;

        protected $guarded = ['id', 'created_at', 'updated_at'];
        protected $casts = [
            'content' => 'array',
            'banner' => 'array',
            'display' => 'bool'
        ];
    }
