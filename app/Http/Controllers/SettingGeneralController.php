<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingGeneralController extends AdminController
{
    protected function fields(){

        return [];
    }

    protected function messages(){
        return [];
    }

    protected $model = 'setting';

    protected function module(){
        return [
            'name' => 'Cấu hình chung',
            'model' => 'settingGeneral',
            'table' =>[
                'name' => 'Tiêu đề',
                'slug' => 'Đường dẫn',
                'display' => 'Hiển thị',
            ]
        ];
    }



    public function index()
    {
        $data['data'] = Setting::firstOrCreate(['name'=>'general']);
        $data['module'] = array_merge($this->module(),[
            'action' => 'update'
        ]);
        return view("admin.module.{$this->module()['model']}.create-edit", $data);
    }



    public function update(Request $request, Setting $settingGeneral)
    {
        $this->validate($request, $this->fields(), $this->messages());

        $input = $request->all();

        $input['content']['index'] = isset($input['content']['index']);

        $settingGeneral->update($input);

        return back()->with($this->flashMessages);
    }
}
