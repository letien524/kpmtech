<?php
$menus = config('admin.menu');
$currentModelSlug = request()->segment(2);
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            @foreach($menus as $model => $menu)
                @if(@$menu['role'] != 'developer')
                    @include('admin.components.menu')
                @else
                    @if(isDeveloper() || isAdmin())
                        @include('admin.components.menu')
                    @endif
                @endif


            @endforeach
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>