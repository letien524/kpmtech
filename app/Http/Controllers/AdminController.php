<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $flashMessages = [
        'flash_level' => 'success',
        'flash_message' => 'Cập nhật dữ liệu thành công!',
    ];

    protected $model;


    public function __construct()
    {
        $this->middleware('auth');

        $model = "\App\\".ucfirst($this->model);
        $this->model = new $model;

    }

    protected function getIdOnUrl(){
        return request()->segment(3);
    }


    public function destroy($id)
    {
        $this->model->destroy($id);
        return back()->with($this->flashMessages);
    }

    public function bulkDestroy(Request $request)
    {
        $input = $request->input('selectItem');

        if (empty($input)) {
            $this->flashMessages = [
                'flash_level' => 'error',
                'flash_message' => 'Bạn chưa chọn dữ liệu cần xóa!',
            ];
            return back()->with($this->flashMessages);
        }

        $this->model->destroy($input);

        return back()->with($this->flashMessages);
    }

    public function ajaxDisplay($id){
        $item = $this->model->find($id);

        if (empty($item)) {
            return response()->json([], 404);
        }
        $item->update(['display'=>!$item['display']]);

        $data = [
            'item' => $item,
            'column' => 'display',
            'module' => [
                'model' => request()->segment(2)
            ],
        ];

        $view = view('admin.components.table-row-loop.display', $data)->render();
        return response()->json(['html' => $view]);
    }

    public function ajaxFeature($id){
        $item = $this->model->find($id);
        if (empty($item)) {
            return response()->json([], 404);
        }
        $item->update(['feature'=>!$item['feature']]);

        $data = [
            'item' => $item,
            'column' => 'feature',
            'module' => [
                'model' => request()->segment(2)
            ],
        ];

        $view = view('admin.components.table-row-loop.feature', $data)->render();
        return response()->json(['html' => $view]);
    }
}
