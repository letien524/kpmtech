<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = ['id','created_at','updated_at'];
    protected $casts = [
        'content' => 'array'
    ];
}
