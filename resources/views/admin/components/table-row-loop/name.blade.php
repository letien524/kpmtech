<td>
    <a href="{{ route("{$module['model']}.edit",$item) }}" title="{{ $item[$column] }}" class="text-default">
        @if(isCurrentRoute('user.index'))
            <img src="{!! __IMAGE_THUMBNAIL_DEFAULT !!}" class="img-responsive imglist lazy"
              data-src="{{imageUrlRender($item['image'])}}"
              alt="{{$item[$column]}}"
            />
        @endif
        <span class="name">{{ $item[$column] }}</span>
    </a>
</td>
