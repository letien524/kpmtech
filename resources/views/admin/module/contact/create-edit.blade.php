@extends('admin.admin')
@section('module.name',$module['name'])
@section('module.page',isUpdate(@$module['action']) ? config('admin.method.edit.display_name') : config('admin.method.create.display_name'))
@section('module.index',route($module['model'].'.index'))
@section('module.show',strlen(@$module['show']) ? $module['show'] : url('/'))

@section('content')
    <form action="{!! updateOrStoreRouteRender(@$module['action'],$module['model'],@$data) !!}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if(isUpdate(@$module['action']))
        {{method_field('put')}}
    @endif
    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1"data-toggle="tab">{!! config('admin.tab.general.display_name') !!}</a></li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab-1">

                    <div class="row">
                        <div class="col-lg-6">

                            <table class="table table-bordered">
                                <tr>
                                    <th>Thời gian</th>
                                    <td>{{$data['created_at']}}</td>
                                </tr>

                                <tr>
                                    <th>Họ tên</th>
                                    <td>{{$data['name']}}</td>
                                </tr>
                                <tr>
                                    <th>Số điện thoại</th>
                                    <td>{{$data['phone']}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$data['email']}}</td>
                                </tr>
                                <tr>
                                    <th>Địa chỉ</th>
                                    <td>{{$data['address']}}</td>
                                </tr>
                                <tr>
                                    <th>Nội dung</th>
                                    <td>{{$data['content']}}</td>
                                </tr>
                            </table>


                        </div>

                        <div class="col-lg-6">
                            <select name="status" class="form-control">
                                <option value="">Mới</option>
                                <option value="1" {{$data['status'] == 1 ? 'selected' : null}}>Đã phản hồi</option>
                            </select>
                        </div>

                    </div>
                </div> <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

        <div class="box box-solid">
            <div class="box-body">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
                @include("admin.components.save-as")
            </div>
        </div>
    </form>
@endsection