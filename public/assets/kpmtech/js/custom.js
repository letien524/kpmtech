function DepthMenuControl(idx,type,obj)
{
	var cachedWidth = $(window).width();
	this.showgnb = function(width)
	{
		$(window).resize( function()
		{
			var newWidth = $(window).width();
			if(newWidth == cachedWidth)
			{
				return;
			}
			cachedWidth = newWidth;
			
			if($('body').width() > 1170)
			{
				$('.gnb').show();
				$('.gnb_button').text('CLOSE');
			}
			else
			{
				$('.gnb').hide();
				$('.gnb_button').text('MENU');
			}
			
			if($('body').width() > 1170 && !($('.gnb').css('display') == 'none'))
			{
				$(window).resize();
			}
		});		
	}
	this.closegnb = function()
	{
		var selecter_all = '.depth02_gnb';
		$(selecter_all).removeClass('focus_menu');
		$('.gnb').hide();
		$('.depth02_gnb').hide();
		$('.gnb_button').text('MENU');
	}

	if(type == 'depth01')
	{
		var selecter_all = '.depth02_gnb';
		var selecter = '.depth02_gnb.depth02_'+idx;
		$(selecter_all).hide();
		$(selecter).show();

		$(selecter).addClass('focus_menu');
		
		if($('body').width() > 1170)
		{
			$(selecter_all).mouseleave(function(event)
			{
				$('.depth02_gnb').hide();
				$(this).removeClass('focus_menu');
			});
		}

		$('.full_container > .container').mouseleave(function(event)
		{
			$('.depth02_gnb').hide();
		});
		
		$('.gnb').mouseleave(function(event)
		{
			if($('body').width() < 768 && $('.gnb').css('display') == 'none')
			{
				$('.gnb').hide();
				$('.gnb_button').text('MENU');
			}
		});		
	}
	if(type == 'depth02')
	{	
		var selecter_all = '.depth02_gnb .depth03';
		var selecter_all_li = '.depth02_gnb nav > ul > li';
			if(obj.parent().hasClass('on'))
			{
				obj.parent().next().hide();	
				obj.parent().removeClass('on');
				return;
			}
			$(selecter_all).hide();
			obj.parent().next().show();	
			$(selecter_all_li).removeClass('on');
			obj.parent().addClass('on');
	}

	if(type == 'mobile')
	{
		if($('.gnb').css('display') == 'none')
		{
			$('.gnb').show();
			obj.text('CLOSE');
			$("html > body .money_list #InsertDiv").removeClass('off_menu');
			$("html > body .money_list #InsertDiv").addClass('on_menu');
		}
		else
		{
			$('.gnb').hide();
			$('.depth02_gnb').hide();
			$('.gnb_button').text('MENU');
			$("html > body .money_list #InsertDiv").addClass('off_menu');
		}
	}
}

function NoticePrevNext(type,post_length,now_length)
{
	if(type == 'next')
	{
		if(now_length+1 <= post_length)
		{
			$(".main_contents .notice .post").hide();
			$(".main_contents .notice .post").eq(now_length+1).show();
		}
	}
	else
	{
		if(now_length-1 >= 0)
		{
			$(".main_contents .notice .post").hide();
			$(".main_contents .notice .post").eq(now_length-1).show();
		}
	}
}

function Product()
{
	if(type == 'next')
	{
		if(now_length+1 <= post_length)
		{
			$(".main_contents .notice .post").hide();
			$(".main_contents .notice .post").eq(now_length+1).show();
		}
	}
	else
	{
		if(now_length-1 >= 0)
		{
			$(".main_contents .notice .post").hide();
			$(".main_contents .notice .post").eq(now_length-1).show();
		}
	}
}


function SubSlideProductList(obj)
{
	var selector = selector = '.sub_contents.design.product dd';

	if(obj.is('button') || obj.is('strong'))
	{
		var is_opend = obj.parent().hasClass('active');
		var parent_check = obj.parent();
		parent_check.parent().find(' > dt.active').removeClass('active');	
	}

	// Ŭ������ ��ü�� ��Ŀ�±� Ȥ�� ������ ��
	if(obj.is('button') || obj.is('strong'))
	{
		if(!is_opend)
		{
			// ��Ŀ�±��� �θ��� �θ��±��� ���� �±װ� ���� �� ���̴� ���� �� ��
			// �ߺ� ������ ����.
			if($(selector+':animated'))
			{
				// ��Ŀ�±��� �θ��� �θ��±��� ���� �±׸� �����̵� �ٿ� ��Ų��.
				parent_check.next().slideDown('600');
				parent_check.addClass('active');

				// ���� �����̵� �� �Ҷ�, ���ļ� �������� �ʵ��� ��ü �±�(?)���ٰ� sliding �̶�� Ŭ������ �߰��Ѵ�.
				$(selector).addClass('sliding');

				// ���� �����̵� �� �Ҷ�, ���ļ� �������� �ʵ��� ��Ŀ�±��� �θ��� �θ��±��� ���� �±׿��ٰ��� sliding�� �����Ѵ�.
				parent_check.next().removeClass('sliding');

				$(selector+'.sliding').slideUp('600',function()
				{
					// ���⼭ ����ϴ� this�� slideUp�� �ϰ� �ִ� '��ü' �̴�.
					// slideUp�� ������, sliding �����͸� �����.
					$(this).removeClass('sliding');
				});
			}
		}
		else
		{
			// ��Ŀ�±��� �θ��� �θ��±��� ���� �±װ� ���� ���̴� ������ ��
			if($(selector+':animated'))
			{
				// ��Ŀ�±��� �θ��� �θ��±��� ���� �±׸� �����̵� �� ��Ų��.
				parent_check.next().slideUp('600');
				parent_check.removeClass('active');
			}
		}
	}
}

function MoneyTableTabBtn()
{
	$(".money_table_btn").click(function()
	{
		$(".money_table_btn").removeClass('on');
		$(this).addClass('on');

		$(".money_table").hide();
		$(".money_table").eq($(this).index()).show();
	});
}

$(document).ready( function()
{
	$(".fmaily_site select").selectbox(
		{
		onChange: function (val, inst){
			if(val.length > 1)
			{
				if(val.indexOf('http:') == -1)
				{
					location.href = val;
					return;
				}
				window.open(val, "_blank"); 
			}
		}
	});
});
$(document).ready( function()
{
	var TabletSizeGnb = new DepthMenuControl();
	if($('body').width() > 1170)
	{
		TabletSizeGnb.showgnb();
	}
	else
	{
		TabletSizeGnb.closegnb();
	}
	MoneyTableTabBtn();
});
