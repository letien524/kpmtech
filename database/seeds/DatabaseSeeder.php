<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*php artisan iseed users,password_resets,blogs,pages,settings,contacts,products,product_categories,category_product --force*/
//        DB::table('users')->insert([
//            ['name'=>'superadmin', 'password' => '$2y$10$.MKAgQw4XCAqnWBGB3ib8O5QsdPPcDTgpotyiap71WN0hvBvoQdQq','level'=>1],
//            ['name'=>'gco_admin', 'password' => bcrypt('1a2s3d4f5g6h'),'level'=>1],
//        ]);
        $this->call(UsersTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(BlogsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);
        $this->call(CategoryProductTableSeeder::class);
    }
}
